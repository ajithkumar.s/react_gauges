import { Range, RangeKeyDict } from "react-date-range";
import { TextInputFieldProps } from "../TextInput/ITextInputFieldProps";

export interface DateRangePickerProps
  extends Omit<TextInputFieldProps, "onChange"> {
  /**
   * startDate?: Date | undefined;
   * endDate?: Date | undefined;
   * color?: string | undefined;
   * key?: string | undefined;
   * autoFocus?: boolean | undefined;
   * disabled?: boolean | undefined;
   * showDateDisplay?: boolean | undefined;
   *
   * @default []
   */
  ranges?: Range;
  onChange: (rangesByKey: RangeKeyDict) => void;
  /**
   * @default `vertical`
   */
  direction?: "horizontal" | "vertical";
  /**
   * @default 1
   */
  months?: number;
  /** @default: true */
  showMonthAndYearPickers?: boolean | undefined;
  /** @default: 20 years after the current date */
  maxDate?: Date | undefined;
  /** @default: 100 years before the current date */
  minDate?: Date | undefined;
  /** @default: `[]` */
  disabledDates?: Date[] | undefined;
  /** @default: true */
  showMonthArrow?: boolean | undefined;
}
