import "./DateRangePicker.scss";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file

import { FunctionComponent, LegacyRef, useRef, useState } from "react";
import { DateRangePickerProps } from "./IDateRangePicker";
import {
  DateRangePicker as DateRangePickers,
  Range,
  DateRange,
} from "react-date-range";
import {
  IconButton,
  Popover,
  TextField,
  Theme,
  useMediaQuery,
} from "@mui/material";
import TextInputField from "../TextInput/TextInputField";
import DateRangeIcon from "@mui/icons-material/DateRange";
import palette from "../../Scss/Variables.module.scss";
import ButtonField from "../Button/ButtonField";
import { dateFormatter } from "../../Utils/dateFormatter";
import IconButtonField from "../Button/IconButtonField";

const DateRangePicker: FunctionComponent<DateRangePickerProps> = (props) => {
  const {
    ranges,
    onChange,
    direction = "horizontal",
    disabled,
    months = 1,
    showMonthAndYearPickers,
    minDate,
    maxDate,
    disabledDates,
    showMonthArrow,
    ...rest
  } = props;
  const buttonRef = useRef<HTMLButtonElement>(null);
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [dateRange, setDateRange] = useState<Range[]>([
    {
      startDate: !!ranges ? ranges.startDate : new Date(),
      // endDate: addDays(new Date(), 7),
      endDate: !!ranges ? ranges.endDate : new Date(),
      key: "selection",
    },
  ]);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  // console.log(!!(ranges && Object.keys(ranges).length !== 0));

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  const smBreackPoint = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("sm")
  );
  // console.log(open);

  const Component = smBreackPoint ? DateRange : DateRangePickers;

  return (
    <>
      <TextInputField
        {...rest}
        value={
          ranges
            ? dateFormatter(ranges.startDate!, "DD/MM/YYYY") +
              " - " +
              dateFormatter(ranges.endDate!, "DD/MM/YYYY")
            : null
        }
        onClick={() => buttonRef.current?.click()}
        readOnly={true}
        InputProps={{
          endAdornment: (
            <IconButton
              ref={buttonRef}
              onClick={disabled ? undefined : handleClick}
            >
              <DateRangeIcon />
            </IconButton>
          ),
        }}
        disabled={disabled}
        shrink={!!(ranges && Object.keys(ranges).length !== 0)}
      />

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        slotProps={{ paper: { sx: { overflow: "auto" } } }}
      >
        <Component
          editableDateInputs={true}
          moveRangeOnFirstSelection={false}
          showMonthAndYearPickers={showMonthAndYearPickers}
          // retainEndDateOnFirstSelection={true}
          onChange={(event) => {
            onChange(event);
            setDateRange([event.selection]);
          }}
          months={months}
          ranges={dateRange}
          direction={direction}
          color={palette.primary}
          rangeColors={[palette.primary]}
          minDate={minDate}
          maxDate={maxDate}
          disabledDates={disabledDates}
          showMonthArrow={showMonthArrow}
        />
        <footer className="date_range_footer">
          <ButtonField className="date_range_ok_button" onClick={handleClose}>
            OK
          </ButtonField>
        </footer>
      </Popover>
    </>
  );
};

export default DateRangePicker;
