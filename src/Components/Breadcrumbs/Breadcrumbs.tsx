import { FunctionComponent } from "react";
import Box from "@mui/material/Box";
import Link, { LinkProps } from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import {
  Link as RouterLink,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import { SvgIcon } from "@mui/material";
import { ReactComponent as dot } from "../../Assets/Images/dot.svg";

interface BreadCrumbsProps {}

const BreadCrumbs: FunctionComponent<BreadCrumbsProps> = () => {
  return (
    <Box sx={{ display: "flex", flexDirection: "column", width: 360 }}>
      <Routes>
        <Route path="*" element={<Page />} />
      </Routes>
    </Box>
  );
};

interface LinkRouterProps extends LinkProps {
  to: string;
  replace?: boolean;
}

function LinkRouter(props: Readonly<LinkRouterProps>) {
  return <Link {...props} component={RouterLink as any} />;
}

function Page() {
  const location = useLocation();
  const pathnames = location.pathname.split("/").filter((x) => x);

  return (
    <Breadcrumbs
      aria-label="breadcrumb"
      separator={
        <SvgIcon
          sx={{
            width: "5px",
            height: "5px",
          }}
          component={dot}
          inheritViewBox
        />
      }
    >
      <LinkRouter underline="hover" color="inherit" to="/app">
        Dashboard
      </LinkRouter>
      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        const to = `/${pathnames.slice(0, index + 1).join("/")}`;
        const breadcrumbNames = to.split("/");
        const breadcrumbName = breadcrumbNames[breadcrumbNames.length - 1];

        return last ? (
          <Typography
            color="text.primary"
            key={to}
            sx={{ textTransform: "capitalize" }}
          >
            {breadcrumbName}
          </Typography>
        ) : (
          <LinkRouter
            underline="hover"
            color="inherit"
            to={to}
            key={to}
            sx={{ textTransform: "capitalize" }}
          >
            {breadcrumbName}
          </LinkRouter>
        );
      })}
    </Breadcrumbs>
  );
}

export default BreadCrumbs;
