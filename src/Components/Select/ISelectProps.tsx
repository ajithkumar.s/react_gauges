import {
  AutocompleteChangeReason,
  AutocompleteFreeSoloValueMapping,
  FilledInputProps,
  InputProps,
  OutlinedInputProps,
  SxProps,
  Theme,
} from "@mui/material";

export interface SelectProps<
  value = AutocompleteOption | string | null,
  FreeSolo = boolean
> extends InputField {
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
  /**
   * The value of the autocomplete.
   *
   * The value must have reference equality with the option in order to be selected.
   * You can customize the equality behavior with the `isOptionEqualToValue` prop.
   */
  value?: value;
  /**
   * The value of the autocomplete.
   *
   * The value must have reference equality with the option in order to be selected.
   * You can customize the equality behavior with the `isOptionEqualToValue` prop.
   */
  defaultValue?: value;
  /**
   * Array of options.
   */
  options?: value[];
  /**
   * This prop is used to help implement the accessibility logic.
   * If you don't provide an id it will fall back to a randomly generated one.
   */
  id?: string;
  /**
   * If `true`, the component is in a loading state.
   * This shows the `loadingText` in place of suggestions (only if there are no suggestions to show, e.g. `options` are empty).
   * @default false
   */
  loading?: boolean;
  /**
   * Text to display when in a loading state.
   *
   * For localization purposes, you can use the provided [translations](/material-ui/guides/localization/).
   * @default 'Loading…'
   */
  loadingText?: React.ReactNode;
  /**
   * Render the option, use `getOptionLabel` by default.
   *
   * @param {object} props The props to apply on the li element.
   * @param {Value} option The option to render.
   * @returns {ReactNode}
   */
  renderOption?: (
    props: React.HTMLAttributes<HTMLLIElement>,
    option: value
  ) => React.ReactNode;
  /**
   * The label content.
   */
  label?: React.ReactNode;
  /**
   * customize the text field by css file
   */
  AutoCompleteClassName?: string;
  /**
   * If `true`, the Autocomplete is free solo, meaning that the user input is not bound to provided options.
   * @default false
   */
  freeSolo?: FreeSolo;
  /**
   * Used to determine the disabled state for a given option.
   *
   * @param {value} option The option to test.
   * @returns {boolean}
   */
  getOptionDisabled?: (option: value) => boolean;
  /**
   * Callback fired when the popup requests to be opened.
   * Use in controlled mode (see open).
   *
   * @param {React.SyntheticEvent} event The event source of the callback.
   */
  onOpen?: (event: React.SyntheticEvent) => void;
  /**
   * Callback fired when the popup requests to be closed.
   * Use in controlled mode (see open).
   *
   * @param {React.SyntheticEvent} event The event source of the callback.
   * @param {string} reason Can be: `"toggleInput"`, `"escape"`, `"selectOption"`, `"removeOption"`, `"blur"`.
   */
  onClose?: (
    event: React.SyntheticEvent,
    reason: AutocompleteCloseReason
  ) => void;
  /**
   * If `true`, the popup won't close when a value is selected.
   * @default false
   */
  disableCloseOnSelect?: boolean;
  /**
   * If `true`, the input can't be cleared.
   * @default false
   */
  disableClearable?: boolean;
  /**
   * If `true`, the component is disabled.
   * @default false
   */
  disabled?: boolean;
  /**
   * If `true`, the component becomes readonly. It is also supported for multiple tags where the tag cannot be deleted.
   * @default false
   */
  readOnly?: boolean;
  /**
   * Used to determine the string value for a given option.
   * It's used to fill the input (and the list box options if `renderOption` is not provided).
   *
   * If used in free solo mode, it must accept both the type of the options and a string.
   *
   * @param {value} option
   * @returns {string}
   * @default (option) => option.label ?? option
   */
  getOptionLabel?: (
    option: value | AutocompleteFreeSoloValueMapping<FreeSolo>
  ) => string;
  /**
   * Callback fired when the focuse was change.
   */
  onBlur?: React.FocusEventHandler<HTMLDivElement>;
  /**
   * Callback fired when the focuse was change.
   */
  onFocus?: React.FocusEventHandler<HTMLDivElement>;
  /**
   * If `true`, the first option is automatically highlighted.
   * @default false
   */
  autoHighlight?: boolean;
  /**
   * If `true`, the input will take up the full width of its container.
   * @default false
   */
  fullWidth?: boolean;
  /**
   * Callback fired when the value changes.
   *
   * @param {React.SyntheticEvent} event The event source of the callback.
   * @param {value|value[]} value The new value of the component.
   * @param {string} reason One of "createOption", "selectOption", "removeOption", "blur" or "clear".
   */
  onChange?: (
    event: React.SyntheticEvent<Element, Event>,
    value: NonNullable<string | AutocompleteOption> | null,
    reason: AutocompleteChangeReason
  ) => void;
  /**
   * Text to display when there are no options.
   *
   * For localization purposes, you can use the provided [translations](/material-ui/guides/localization/).
   * @default 'No options'
   */
  noOptionsText?: React.ReactNode;
  /**
   * If `true`, hide the selected options from the list box.
   * @default false
   */
  filterSelectedOptions?: boolean;
}

export type AutocompleteOption = {
  label?: string;
  value?: string | number;
};

export type AutocompleteCloseReason =
  | "createOption"
  | "toggleInput"
  | "escape"
  | "selectOption"
  | "removeOption"
  | "blur";

interface InputField {
  /**
   * The id of the `input` element.
   * Use this prop to make `label` and `helperText` accessible for screen readers.
   */
  id?: string;
  /**
   * Name attribute of the `input` element.
   */
  name?: string;
  /**
   * The short hint displayed in the `input` before the user enters a value.
   */
  placeholder?: string;
  /**
   * The variant to use.
   * @default 'outlined'
   */
  variant?: "outlined" | "standard" | "filled";
  /**
   * The label content.
   */
  label?: React.ReactNode;
  /**
   * The color of the component.
   * @default 'primary'
   */
  color?: "error" | "primary" | "secondary" | "info" | "success" | "warning";
  /**
   * If `true`, the label is displayed as required and the `input` element is required.
   * @default false
   */
  required?: boolean;
  /**
   * customize the text field by css file
   */
  className?: string;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  inputSx?: SxProps<Theme>;
  /**
   * If true, the label is displayed in an error state
   * @default false
   */
  error?: boolean;
  /**
   * customize the input by css file
   */
  inputClassName?: string;
  /**
   * customize the lable by css file
   */
  inputLableStyle?: string;
  /**
   * The helper text content.
   * It's uses for showing error text
   */
  helperText?: React.ReactNode;
  /**
   * Props applied to the Input element.
   * It will be a [`FilledInput`](/material-ui/api/filled-input/),
   * [`OutlinedInput`](/material-ui/api/outlined-input/) or [`Input`](/material-ui/api/input/)
   * component depending on the `variant` prop value.
   */
  InputProps?:
    | Partial<OutlinedInputProps>
    | Partial<InputProps>
    | Partial<FilledInputProps>;
  /**
   * If `true`, the label is shrunk.
   */
  shrink?: boolean;
  /**
   * The size of the component.
   * @default 'small'
   */
  size?: "small" | "medium";
  /**
   * The size of the component.
   * @default 'normal'
   */
  labelSize?: "small" | "normal";
}
