import { IMenuItem } from "./ISidnavbar";
import { IconButton, SvgIcon, Typography } from "@mui/material";
import { ReactComponent as App } from "../../Assets/Images/app.svg";
import { ReactComponent as Booking } from "../../Assets/Images/booking.svg";
import Avatars from "../Avatar/Avatars";
import CloseIcon from "@mui/icons-material/Close";

// managment

/*    dummy menu     */
export const menu: { [key: string]: IMenuItem[] } = {
  Dashboard: [
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          component={App}
          inheritViewBox
        />
      ),
      title: "Dashboard",
      to: "dashboard",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
    },
  ],
  Masters: [
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          component={Booking}
          inheritViewBox
        />
      ),
      title: "User Permission",
      to: "userPermission",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
    },
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          component={App}
          inheritViewBox
        />
      ),
      title: "Product",
      to: "product",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
      items: [
        {
          title: "Sub Product",
          to: "sub",
          permissions: {
            add: true,
            view: true,
            edit: true,
            delete: true,
          },
          items: [
            {
              title: "Sub Product",
              to: "product/sub",
              permissions: {
                add: true,
                view: true,
                edit: true,
                delete: true,
              },
            },
          ],
        },
        {
          title: "Fundamental Analysis",
          to: "product/fun",
          permissions: {
            add: true,
            view: true,
            edit: true,
            delete: true,
          },
        },
      ],

      // Nested Menu Items

      // items: [
      //   {
      //     title: "Sub Product",
      //     items: [
      //       {
      //         title: "The Dow Theory",
      //         to: "product",
      //         permissions: {
      //           add: true,
      //           view: true,
      //           edit: true,
      //           delete: true,
      //         },
      //       },
      //       {
      //         title: "Charts & Chart Patterns",
      //         to: "Ecommerce",
      //       },
      //       {
      //         title: "Trend & Trend Lines",
      //         to: "/trendlines",
      //       },
      //       {
      //         title: "Support & Resistance",
      //         to: "/sandr",
      //       },
      //     ],
      //   },
      //   {
      //     title: "Fundamental Analysis",
      //     items: [
      //       {
      //         title: "The Dow Theory",
      //         to: "/thedowtheory",
      //       },
      //       {
      //         title: "Charts & Chart Patterns",
      //         to: "/chart",
      //       },
      //       {
      //         title: "Trend & Trend Lines",
      //         to: "/trendlines",
      //       },
      //       {
      //         title: "Support & Resistance",
      //         to: "/sandr",
      //       },
      //     ],
      //   },
      //   {
      //     title: "Elliot Wave Analysis",
      //     items: [
      //       {
      //         title: "The Dow Theory",
      //         to: "/thedowtheory",
      //       },
      //       {
      //         title: "Charts & Chart Patterns",
      //         to: "/chart",
      //       },
      //       {
      //         title: "Trend & Trend Lines",
      //         to: "/trendlines",
      //       },
      //       {
      //         title: "Support & Resistance",
      //         to: "/sandr",
      //       },
      //     ],
      //   },
      // ],
    },
  ],
  Transactions: [
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          // component={Booking}
          inheritViewBox
        />
      ),
      title: "Transactions",
      to: "transactions",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
    },
  ],
  Reports: [
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          // component={Booking}
          inheritViewBox
        />
      ),
      title: "Reports",
      to: "reports",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
    },
  ],
  Settings: [
    {
      icon: (
        <SvgIcon
          sx={{
            width: "20px",
            height: "20px",
          }}
          // component={Booking}
          inheritViewBox
        />
      ),
      title: "Settings",
      to: "setting",
      permissions: {
        add: true,
        view: true,
        edit: true,
        delete: true,
      },
    },
  ],
};

export const skipedMenu = ["/"];

export const notificationMenu = [
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
  {
    content: (
      <div
        style={{
          display: "flex",
          gap: "10px",
          alignItems: "center",
        }}
      >
        <Avatars></Avatars>
        <div style={{}}>
          <Typography component={"div"}>New Message from A</Typography>
          <Typography component={"p"} variant="body2">
            Feb 28 10:50 am
          </Typography>
        </div>
        <IconButton>
          <SvgIcon
            sx={{
              height: "20px",
              width: "20px",
            }}
            component={CloseIcon}
          />
        </IconButton>
      </div>
    ),
  },
];
