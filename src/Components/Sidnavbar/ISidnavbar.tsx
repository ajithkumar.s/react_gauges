import { NavLinkProps } from "react-router-dom";

export interface IMenuItem {
  icon?: React.ReactNode;
  items?: IMenuItem[];
  title?: string;
  to?: string;
  permissions?: {
    add?: boolean;
    view?: boolean;
    edit?: boolean;
    delete?: boolean;
  };
}

export interface INavLink extends NavLinkProps {
  activeClassName?: string;
}
