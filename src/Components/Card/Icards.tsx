import { SxProps, Theme } from "@mui/material";

export interface CardsProps {
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
  /**
   * If `true`, the card will use raised styling.
   * @default false
   */
  raised?: boolean;
  /**
   * The variant to use.
   * @default 'elevation'
   */
  variant?: "elevation" | "outlined";
  /**
   * If `true`, rounded corners are disabled.
   * @default false
   */
  square?: boolean;
  className?: string;
  children?: React.ReactNode;
}
