import { CardMediaOwnProps, SxProps, Theme } from "@mui/material";

export interface CardMediasProps extends CardMediaOwnProps {
  children?: React.ReactNode;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
}
