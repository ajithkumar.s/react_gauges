import { CardMedia } from "@mui/material";
import { FunctionComponent } from "react";
import { CardMediasProps } from "./ICardMedia";

const CardMedias: FunctionComponent<CardMediasProps> = (props) => {
  const { sx, children } = props;
  return <CardMedia sx={sx}>{children}</CardMedia>;
};

export default CardMedias;
