import { CardActions } from "@mui/material";
import { FunctionComponent } from "react";
import { CardActionProps } from "./ICardActions";

const CardAction: FunctionComponent<CardActionProps> = (props) => {
  const { sx, disableSpacing = false, className, children } = props;
  return (
    <CardActions sx={sx} disableSpacing={disableSpacing} className={className}>
      {children}
    </CardActions>
  );
};

export default CardAction;
