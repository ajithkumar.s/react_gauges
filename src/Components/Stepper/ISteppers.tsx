export interface IStepperProps {
  /**
   * Array of string value which display on each step title.
   */
  steps?: string[];
  /**
   * Array of number to set steps to optional which having skip button.
   */
  optionalSteps?: number[];
  /**
   * Step Component
   */
  stepComponents?: IStepComponent[];
  onNext?: () => void;
  /**
   * If set the `Stepper` will not assist in controlling steps for linear flow.
   * @default false
   */
  nonLinear?: boolean;
}
export interface IStepComponent {
  component?: React.ReactNode;
  nextAction?: () => void;
  nextButtonDisable?: boolean;
}
