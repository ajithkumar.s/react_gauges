import {
  FilledInputProps,
  InputProps,
  OutlinedInputProps,
  Theme,
  SxProps,
} from "@mui/material";

export interface TextInputFieldProps {
  /**
   * The id of the `input` element.
   * Use this prop to make `label` and `helperText` accessible for screen readers.
   */
  id?: string;
  /**
   * Name attribute of the `input` element.
   */
  name?: string;
  /**
   * The short hint displayed in the `input` before the user enters a value.
   */
  placeholder?: string;
  /**
   * The variant to use.
   * @default 'outlined'
   */
  variant?: "outlined" | "standard" | "filled";
  /**
   * The label content.
   */
  label?: React.ReactNode;
  /**
   * The color of the component.
   * @default 'primary'
   */
  color?: "error" | "primary" | "secondary" | "info" | "success" | "warning";
  /**
   * If `true`, the label is displayed as required and the `input` element is required.
   * @default false
   */
  required?: boolean;
  /**
   * customize the text field by css file
   */
  className?: string;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
  /**
   * The value of the `input` element, required for a controlled component.
   */
  value?: unknown;
  /**
   * The default value. Use when the component is not controlled.
   */
  defaultValue?: unknown;
  /**
   * If true, the label is displayed in an error state
   * @default false
   */
  error?: boolean;
  /**
   * if true the component will be disabled
   *@default false
   */
  disabled?: boolean;
  /**
   * It prevents the user from changing the value of the field
   * (not from interacting with the field).
   * @default false
   */
  readOnly?: boolean;
  /**
   * If `true`, the input will take up the full width of its container.
   * @default false
   */
  fullWidth?: boolean;
  /**
   * Type of the `input` element. It should only allow  "color" | "email" | "password" | "text" | "search".
   * @default 'text'
   */
  type?: "color" | "email" | "password" | "text" | "search";
  /**
   * customize the input by css file
   */
  inputClassName?: string;
  /**
   * customize the lable by css file
   */
  inputLableStyle?: string;
  /**
   * The helper text content.
   * It's uses for showing error text
   */
  helperText?: React.ReactNode;
  /**
   * Props applied to the Input element.
   * It will be a [`FilledInput`](/material-ui/api/filled-input/),
   * [`OutlinedInput`](/material-ui/api/outlined-input/) or [`Input`](/material-ui/api/input/)
   * component depending on the `variant` prop value.
   */
  InputProps?:
    | Partial<OutlinedInputProps>
    | Partial<InputProps>
    | Partial<FilledInputProps>;
  /**
   * If `true`, the label is shrunk.
   */
  shrink?: boolean;
  /**
   * The size of the component.
   * @default 'small'
   */
  size?: "small" | "medium";
  /**
   * The size of the component.
   * @default 'normal'
   */
  labelSize?: "small" | "normal";
  /**
   * Callback fired when the value is changed.
   *
   * @param {object} event The event source of the callback.
   * You can pull out the new value by accessing `event.target.value` (string).
   */
  onChange?: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  /**
   * Callback fired when the focuse was change.
   */
  onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  /**
   * Callback fired when the focuse was change.
   */
  onFocus?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
}
