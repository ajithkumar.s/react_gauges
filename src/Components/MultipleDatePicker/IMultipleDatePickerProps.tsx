import { HTMLAttributes } from "react";
import { CustomComponentProps, DateObject } from "react-multi-date-picker";

export interface MultipleDatePickerProps {
  /**
   * Change the defalut error message of the zod validation
   */
  customErrorMessage?: string;
  /**
   * Input name.
   * This feature does not work if you render custom input.
   */
  name: string;
  id?: string;
  title?: string;
  /**
   * DatePicker container style.
   */
  containerStyle?: React.CSSProperties;
  /**
   * DatePicker container className.
   */
  containerClassName?: string;
  pickerLable?: string;
  /**
   * Return `false` in case you don't want to open Calendar
   */
  onOpen?(): void | boolean;
  /**
   * Return `false` in case you don't want to close Calendar
   */
  onClose?(): void | boolean;
  /**
   * Input placeholder.
   * This feature does not work if you render custom input.
   */
  placeholder?: string;
  onPropsChange?(props: object): void;
  onMonthChange?(date: DateObject): void;
  onYearChange?(date: DateObject): void;
  onFocusedDateChange?(
    focusedDate: DateObject | undefined,
    clickedDate: DateObject | undefined
  ): void;
  /**
   * @example
   * <DatePicker
   *   render={<CustomComponent/>}
   * />
   */
  render?:
    | React.ReactElement<CustomComponentProps>
    | ((
        value: string,
        openCalendar: () => void,
        handleValueChange: (e: React.ChangeEvent) => void,
        locale: Locale,
        separator: string
      ) => React.ReactNode);
  /**
   * @example
   * <Calendar
   *  value={[new Date(2020,10,10), new Date(2020,10,14)]}
   *  multiple
   * />
   *
   * <DatePicker multiple />
   */
  multiple?: boolean;
  /**
   * @example
   * <Calendar
   *  value={[new Date(2020,10,10), new Date(2020,10,14)]}
   *  range
   * />
   *
   * <DatePicker range />
   */
  range?: boolean;
  /**
   * @type string
   * @default "YYYY/MM/DD"
   * @see https://shahabyazdi.github.io/react-multi-date-picker/format-tokens/
   * @example
   * <Calendar format="MM/DD/YYYY hh:mm:ss a" />
   *
   * <DatePicker format="MM-DD-YYYY HH:mm:ss" />
   */
  format?: string;
  onlyMonthPicker?: boolean;
  onlyYearPicker?: boolean;
  /**
   * default calendar is gregorian.
   *
   * if you want to use other calendars instead of gregorian,
   * you must import it from "react-date-object"
   *
   * Availble calendars:
   *
   *   - gregorian
   *   - persian
   *   - arabic
   *   - indian
   *
   * @example
   * import persian from "react-date-object/calendars/persian"
   * <Calendar calendar={persian} />
   *
   * import indian from "react-date-object/calendars/indian"
   * <DatePicker calendar={indian} />
   */
  calendar?: Calendar;
  /**
   * default locale is gregorian_en.
   *
   * if you want to use other locales instead of gregorian_en,
   * you must import it from "react-date-object"
   *
   * Availble locales:
   *
   *  - gregorian_en
   *  - gregorian_fa
   *  - gregorian_ar
   *  - gregorian_hi
   *  - persian_en
   *  - persian_fa
   *  - persian_ar
   *  - persian_hi
   *  - arabic_en
   *  - arabic_fa
   *  - arabic_ar
   *  - arabic_hi
   *  - indian_en
   *  - indian_fa
   *  - indian_ar
   *  - indian_hi
   *
   * @example
   * import gregorian_fa from "react-date-object/locales/gregorian_fa"
   * <Calendar locale={gregorian_fa} />
   *
   * import gregorian_ar from "react-date-object/locales/gregorian_ar"
   * <DatePicker locale={gregorian_ar} />
   */
  locale?: Locale;
  /**
   * You can customize your calendar days
   * with the mapDays Prop and create different properties
   * for each of them by returning the Props you want.
   * @see https://shahabyazdi.github.io/react-multi-date-picker/map-days/
   * @example
   * <DatePicker
   *  mapDays={() => {
   *    return {
   *      style: {
   *        backgroundColor: "brown",
   *        color: "white"
   *      }
   *    }
   *  }}
   * />
   */
  mapDays?(object: {
    date: DateObject;
    selectedDate: DateObject | DateObject[];
    currentMonth: object;
    isSameDate(arg1: DateObject, arg2: DateObject): boolean;
  }):
    | (HTMLAttributes<HTMLSpanElement> & {
        disabled?: boolean;
        hidden?: boolean;
      })
    | void;
  /**
   * Calendar z-index
   * @default 100
   */
  zIndex?: number;
  /**
   * Availble Positions:
   *  - top
   *  - bottom
   *  - left
   *  - right
   *
   * @example
   *
   * <DatePicker
   *  plugins={[
   *      <ImportedPlugin position="right" />
   *  ]}
   * />
   */
  plugins?: (Plugin | Plugin[])[];
  onChange?(
    date: DateObject | DateObject[] | null,
    options: {
      validatedValue: string | Array<string>;
      input: HTMLElement;
      isTyping: boolean;
    }
  ): void | false;
  dateSeparator?: string;
  multipleRangeSeparator?: string;
  typingTimeout?: number;
  ref?: React.MutableRefObject<any>;
  /**
   * @example
   * <DatePicker
   *   format="Date:YYYY/MM/DD, Time:HH:mm:ss"
   *   formattingIgnoreList={["Date", "Time"]}
   * />
   */
  formattingIgnoreList?: string[];
  /**
   * @types Date | string | number | DateObject
   * @types Date[] | string[] | number[] | DateObject[]
   * @example
   * <Calendar value={new Date()} />
   * <DatePicker value={[new Date(), new Date(2020, 2, 12)]} />
   */
  value?: Value;

  /**
   * In Multiple mode, use this Prop to sort the selected dates.
   *
   * @example
   *
   * <DatePicker multiple sort />
   */
  sort?: boolean;
  /**
   * Calendar wrapper className
   */
  className?: string;
  /**
   * @see https://shahabyazdi.github.io/react-multi-date-picker/locales/
   * @example
   * const weekDays = ["S", "M", "T", "W", "T", "F", "S"]
   * <DatePicker
      weekDays={weekDays}
    />
   */
  weekDays?: string[] | Array<string[]>;
  /**
   * @see https://shahabyazdi.github.io/react-multi-date-picker/locales/
   * @example
   * <Calendar
   *  months={[
   *    "jan",
   *    "feb",
   *    "mar",
   *    "apr",
   *    "may",
   *    "jun",
   *    "jul",
   *    "aug",
   *    "sep",
   *    "oct",
   *    "nov",
   *    "dec"
   *  ]}
   * />
   */
  months?: string[] | Array<string[]>;
  showOtherDays?: boolean;
  /**
   * the date you set in datepicker as value must be equal or bigger than min date.
   *
   * otherwise datepicker recognise it as `invalid date` and doesn't format it.
   *
   * @example
   * <DatePicker
   *  value="2020/12/05"
   *  minDate="2020/12/05"
   * />
   */
  minDate?: Date | string | number | DateObject;
  /**
   * the date you set in datepicker as value must be equal or smaller than max date.
   *
   * otherwise datepicker recognise it as `invalid date` and doesn't format it.
   *
   * @example
   * <DatePicker
   *  value="2020/12/01"
   *  maxDate="2020/12/06"
   * />
   */
  maxDate?: Date | string | number | DateObject;
  disableDayPicker?: boolean;
  disableMonthPicker?: boolean;
  disableYearPicker?: boolean;
  numberOfMonths?: number;
  /**
   * const digits = ["๐", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙"]
   * <DatePicker
      digits={digits}
    />
   */
  digits?: string[];
  currentDate?: DateObject;
  /**
   * You can set the buttons prop to false to disable the previous & next buttons.
   *
   * @example
   * <Calendar buttons={false} />
   */
  buttons?: boolean;
  /**
   * You can render your favorite element instead of the previous & next buttons.
   *
   * @example
   * <Calendar renderButton={<CustomButton />} />
   */
  renderButton?: React.ReactElement | Function;
  weekStartDayIndex?: number;
  readOnly?: boolean;
  disabled?: boolean;
  hideMonth?: boolean;
  hideYear?: boolean;
  hideWeekDays?: boolean;
  shadow?: boolean;
  fullYear?: boolean;
  rangeHover?: boolean;
  highlightToday?: boolean;
  /**
   * Input style.
   * This feature does not work if you render custom input.
   */
  style?: React.CSSProperties;
  /**
   * This feature does not work if you render custom input.
   *
   * You can also use this prop for button and icon type.
   *
   * Default class names:
   *
   *  - input : `rmdp-input`
   *
   *  - button : `rmdp-button`
   *
   * Note that when you enter a new className, the default className is automatically `removed`.
   */
  inputClassName?: string;
}

export type FunctionalPlugin = { type: string; fn: Function };
export type Plugin = React.ReactElement | FunctionalPlugin;
export type Value =
  | Date
  | string
  | number
  | DateObject
  | Date[]
  | string[]
  | number[]
  | DateObject[]
  | null;

export type Calendar = {
  name: string;
  /**
   * specifies what number the calendar year begins with.
   * some calendars start with year 0
   */
  startYear: number;
  yearLength: number;
  epoch: number;
  century: number;
  weekStartDayIndex: number;
  /**
   *
   * @param isLeap
   *
   * return an array containing the number of days in each month.
   */
  getMonthLengths(isLeap: boolean): number[];
  isLeap(year: number): boolean;
  /**
   *
   * @param currentYear
   *
   * returns all leap years including the current year
   * (if the current year is a leap year).
   */
  getLeaps(currentYear: number): number[] | [];
  getDayOfYear(date: DateObject): number;
  getAllDays(date: DateObject): number;
  /**
   *
   * @param year
   *
   * Returns the number of leap years before the specified year.
   */
  leapsLength?(year: number): number;
  guessYear(days: number, currentYear: number): number;
};

export type Locale = {
  name: string;
  months: Array<string[]>;
  weekDays: Array<string[]>;
  digits: string[];
  meridiems: Array<string[]>;
};
