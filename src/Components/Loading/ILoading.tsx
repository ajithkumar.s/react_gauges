export interface LoadingProps {
  /**
   * If `true`, the component is shown.
   */
  isLoading: boolean;
  /**
   * The color of the component.
   * It supports both default and custom theme colors, which can be added as shown in the
   * @default 'primary'
   */
  circularColor?:
    | "primary"
    | "secondary"
    | "error"
    | "info"
    | "success"
    | "warning"
    | "inherit";
  /**
   * The color of the component.
   * It supports both default and custom theme colors, which can be added as shown in the
   * @default 'primary'
   */
  backgroundColor?:
    | "primary"
    | "secondary"
    | "error"
    | "info"
    | "success"
    | "warning"
    | "inherit";
  circularClassName?: string;
}
