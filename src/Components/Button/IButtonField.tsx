import { SxProps, Theme } from "@mui/material";
import { ChangeEvent } from "react";

export interface ButtonFieldProps {
  children?: React.ReactNode;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
  /**
   * If `true`, the component is disabled.
   * @default false
   */
  disabled?: boolean;
  /**
   * The variant to use.
   * @default 'text'
   */
  variant?: "text" | "outlined" | "contained";
  /**
   * Callback funtion trigers when the button click
   */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  /**
   * The color of the component.
   * It supports both default and custom theme colors, which can be added as shown in the
   * [palette customization guide](https://mui.com/material-ui/customization/palette/#custom-colors).
   * @default 'primary'
   */
  color?:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning";
  /**
   * The size of the component.
   * `small` is equivalent to the dense button styling.
   * @default 'medium'
   */
  size?: "small" | "medium" | "large";
  /**
   * Element placed before the children.
   */
  startIcon?: React.ReactNode;
  /**
   * Element placed after the children.
   */
  endIcon?: React.ReactNode;
  /**
   * If `true`, the ripple effect is disabled.
   *
   * ⚠️ Without a ripple there is no styling for :focus-visible by default. Be sure
   * to highlight the element by applying separate styles with the `.Mui-focusVisible` class.
   * @default false
   */
  disableRipple?: boolean;

  className?: string;
  /**
   *
   */
  type?: "submit" | "button" | "reset";
  ref?:
    | ((instance: HTMLButtonElement | null) => void)
    | React.RefObject<HTMLButtonElement>
    | null
    | undefined;
}

export interface LoadingButtonFieldProps extends ButtonFieldProps {
  /**
   * If `true`, the loading indicator is shown and the button becomes disabled.
   * @default false
   */
  loading?: boolean;
  /**
   * Element placed before the children if the button is in loading state.
   * The node should contain an element with `role="progressbar"` with an accessible name.
   * By default we render a `CircularProgress` that is labelled by the button itself.
   * @default <CircularProgress color="inherit" size={16} />
   */
  loadingIndicator?: React.ReactNode;
  /**
   * The loading indicator can be positioned on the start, end, or the center of the button.
   * @default 'center'
   */
  loadingPosition?: "start" | "end" | "center";
}
type AcceptedFileType =
  | "image/jpeg"
  | "image/png"
  | "image/gif"
  | "image/bmp"
  | "image/webp"
  | "image/svg+xml"
  | "image/tiff"
  | "application/pdf"
  | "application/msword"
  | "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  | "application/vnd.ms-excel"
  | "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  | "application/vnd.ms-powerpoint"
  | "application/vnd.openxmlformats-officedocument.presentationml.presentation"
  | "application/vnd.oasis.opendocument.text"
  | "application/vnd.oasis.opendocument.spreadsheet"
  | "application/vnd.oasis.opendocument.presentation"
  | "application/rtf"
  | "audio/mpeg"
  | "audio/ogg"
  | "audio/wav"
  | "audio/midi"
  | "video/mp4"
  | "video/mpeg"
  | "video/ogg"
  | "video/webm"
  | "text/plain"
  | "text/html"
  | "text/css"
  | "application/json"
  | "application/xml"
  | "application/zip"
  | "application/x-gzip"
  | "application/x-tar"
  | "application/x-bzip2"
  | "application/octet-stream";

export interface UploadButtonFieldProps extends ButtonFieldProps {
  /**
   * File upload onchange function.
   *Provides uploaded files details every file choose.
   */
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  /**
   * Input element id attribute
   */
  id?: string;
  /**
   * Input element name attribute
   */
  name?: string;
  /**
   * Set the file accept type in the input element attribute
   */
  fileAccept?: AcceptedFileType | string;
  onBlur?: React.FocusEventHandler<HTMLInputElement>;
  value?: string | number | readonly string[];
  /**
   * Custom Error message helps to provide error message incase of undefined error message arise in validation
   *
   */
  customErrorMessage?: string;
}
