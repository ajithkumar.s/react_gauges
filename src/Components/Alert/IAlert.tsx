import { AlertProps, SxProps, Theme } from "@mui/material";

export interface IAlertProps extends AlertProps {
  title?: string;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  titleSx?: SxProps<Theme>;
  titleClassName?: string;
}
