import "./Alert.scss";

import { AlertTitle, Alert as MuiAlert } from "@mui/material";
import { FunctionComponent } from "react";
import { IAlertProps } from "./IAlert";
import ButtonField from "../Button/ButtonField";

const Alert: FunctionComponent<IAlertProps> = (props) => {
  const {
    icon,
    severity = "success",
    variant = "filled",
    color = "success",
    action = (
      <ButtonField color="inherit" size="small">
        UNDO
      </ButtonField>
    ),
    title,
    titleSx,
    titleClassName,
    children,
  } = props;
  return (
    <MuiAlert
      icon={icon}
      severity={severity}
      variant={variant}
      color={color}
      action={action}
    >
      {title && (
        <AlertTitle sx={titleSx} className={titleClassName}>
          {title}
        </AlertTitle>
      )}
      {children}
    </MuiAlert>
  );
};

export default Alert;
