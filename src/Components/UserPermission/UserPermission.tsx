import React, { useMemo, useRef, FunctionComponent } from "react";
import Table from "../Table/Table";
import { Columns } from "../Table/ITable";
import CustomToolbar from "../Table/components/CustomToolbar";
import { GetRoleOptionType, UserPermissionProps } from "./IUserPermission";
import "./userPermission.scss";
import CustomFooter from "../Table/components/CustomFooter";
import ButtonField from "../Button/ButtonField";
import Checkbox from "../Checkbox/Checkbox";
import Select from "../withoutControllerComponents/Select/Select";

const UserPermission: FunctionComponent<UserPermissionProps> = (props) => {
  const { rows, RoleOptions, getRoleOption, getRoleValues, sx } = props;

  // Column
  const column: Columns = [
    {
      field: "menuname",
      renderHeader: () => <b>Menus</b>,
      flex: 3,
      type: "string",
      align: "left",
      headerAlign: "center",
      maxWidth: 250,
    },
    {
      field: "view",
      renderHeader: () => <b>View</b>,
      flex: 1,
      type: "boolean",
      sortable: false,
      renderCell(params) {
        return (
          <div>
            <Checkbox
              color="success"
              checked={params.value}
              onChange={(event) => {
                const checked = event.target.checked;
                params.api.updateRows([
                  {
                    ...params.row,
                    view: checked,
                    add: checked && params.row.add,
                    edit: checked && params.row.edit,
                    delete: checked && params.row.delete,
                  },
                ]);
              }}
            />
          </div>
        );
      },
      headerAlign: "center",
      align: "center",
    },
    {
      field: "add",
      renderHeader: () => <b>Add</b>,
      flex: 1,
      type: "boolean",
      headerAlign: "center",
      align: "center",
      sortable: false,
      renderCell(params) {
        return (
          <div>
            <Checkbox
              color="success"
              checked={params.value}
              onChange={(event) => {
                const checked = event.target.checked;
                params.api.updateRows([
                  {
                    ...params.row,
                    add: checked,
                    view: checked || params.row.view,
                  },
                ]);
              }}
            />
          </div>
        );
      },
    },
    {
      field: "edit",
      renderHeader: () => <b>Edit</b>,
      flex: 1,
      type: "boolean",
      headerAlign: "center",
      align: "center",
      sortable: false,
      renderCell(params) {
        return (
          <div>
            <Checkbox
              color="success"
              checked={params.value}
              onChange={(event) => {
                const checked = event.target.checked;
                params.api.updateRows([
                  {
                    ...params.row,
                    edit: checked,
                    view: checked || params.row.view,
                  },
                ]);
              }}
            />
          </div>
        );
      },
    },
    {
      field: "delete",
      renderHeader: () => <b>Delete</b>,
      flex: 1,
      type: "boolean",
      headerAlign: "center",
      align: "center",
      sortable: false,
      renderCell(params) {
        return (
          <div>
            <Checkbox
              color="success"
              checked={params.value}
              onChange={(event) => {
                const checked = event.target.checked;
                params.api.updateRows([
                  {
                    ...params.row,
                    delete: checked,
                    view: checked || params.row.view,
                  },
                ]);
              }}
            />
          </div>
        );
      },
    },
  ];

  // Function helps to get updated permission values
  function useApiRef() {
    const apiRef = useRef<any>(null);
    const _columns = useMemo(
      () =>
        column.concat({
          field: "",
          width: 0,
          minWidth: 0,
          maxWidth: 0,
          flex: 0,
          headerClassName: "hidden_column",
          renderCell: (params: any) => {
            apiRef.current = params.api;
            return null;
          },
        }),

      [apiRef]
    );

    return { apiRef, columns: _columns };
  }
  const { apiRef, columns } = useApiRef();

  // Handle Role update submitt
  const handleSubmitButton = () => {
    const rowModels = apiRef.current.getRowModels();
    const ConvertedRows = Array.from(rowModels.entries()).map((value: any) => {
      return { ...value[1] };
    });
    // console.log(ConvertedRows);
    getRoleValues(ConvertedRows);
  };

  return (
    <Table
      rows={rows}
      columns={columns}
      sx={{
        ...sx,
        "& .css-axafay-MuiDataGrid-virtualScroller": {
          overflowX: "hidden",
        },
      }}
      // columnVisibilityModel={{ "": false }}
    >
      <CustomToolbar className="top_toolbar">
        <Select
          className="select_option"
          options={RoleOptions}
          onChange={(_, v) => getRoleOption(v as GetRoleOptionType)}
          label="Select Roll"
          size="small"
          sx={{ minWidth: "300px" }}
        />
      </CustomToolbar>
      <CustomFooter className="bottom_toolbar">
        <ButtonField variant="contained" onClick={handleSubmitButton}>
          Submit
        </ButtonField>
      </CustomFooter>
    </Table>
  );
};

export default UserPermission;
