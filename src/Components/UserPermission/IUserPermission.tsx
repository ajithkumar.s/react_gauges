import { GridRowsProp } from "@mui/x-data-grid";
import { SxProps, Theme } from "@mui/system";
import { AutocompleteOption } from "../Select/ISelectProps";

export type Rows = GridRowsProp;
export type GetRoleOptionType = {
  lable: string;
  value: number | string;
};

export interface UserPermissionProps {
  rows: Rows;
  sx?: SxProps<Theme>;
  getRoleOption: (v: GetRoleOptionType) => void;
  getRoleValues: (v: any) => void;
  RoleOptions: (AutocompleteOption | string | null)[];
}
export interface ICustomFooter {
  children?: React.ReactNode;
  sx?: SxProps<Theme>;
  className?: string;
}
