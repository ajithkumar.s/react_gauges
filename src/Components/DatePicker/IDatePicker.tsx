import {
  FilledInputProps,
  InputProps,
  OutlinedInputProps,
  SxProps,
  Theme,
} from "@mui/material";
import {
  DateValidationError,
  PickerChangeHandlerContext,
} from "@mui/x-date-pickers";
import { Dayjs } from "dayjs";

export type DateT = Date | Dayjs | string | null;

export interface DatePickerProps<
  TView = "day" | "month" | "year",
  TDate = DateT
> {
  /**
   * Name attribute used by the `input` element in the Field.
   */
  name: string;
  /**
   * If `true`, disable values before the current date for date components, time for time components and both for date time components.
   * @default false
   */
  disablePast?: boolean;
  /**
   * If `true`, disable values after the current date for date components, time for time components and both for date time components.
   * @default false
   */
  disableFuture?: boolean;
  /**
   * If `true`, the open picker button will not be rendered (renders only the field).
   * @default false
   */
  disableOpenPicker?: boolean;
  /**
   * If `true`, the picker and text field are disabled.
   * @default false
   */
  disabled?: boolean;
  /**
   * The label content.
   */
  label?: React.ReactNode;
  /**
   * Pass a ref to the `input` element.
   */
  inputRef?: React.Ref<HTMLInputElement>;
  /**
   * If `true`, today's date is rendering without highlighting with circle.
   * @default false
   */
  disableHighlightToday?: boolean;
  /**
   * If `true`, the week number will be display in the calendar.
   */
  displayWeekNumber?: boolean;
  readOnly?: boolean;
  /**
   * Force rendering in particular orientation.
   */
  orientation?: "portrait" | "landscape";
  /**
   * Available views.
   */
  views?: readonly TView[];
  /**
   * The helper text content.
   */
  helperText?: React.ReactNode;
  /**
   * If `true`, the popover or modal will close after submitting the full date.
   * @default `true` for desktop, `false` for mobile (based on the chosen wrapper and `desktopModeMediaQuery` prop).
   */
  closeOnSelect?: boolean;
  /**
   * Control the popup or dialog open state.
   * @default false
   */
  open?: boolean;
  /**
   * Callback fired when the popup requests to be closed.
   * Use in controlled mode (see `open`).
   */
  onClose?: () => void;
  /**
   * Callback fired when the popup requests to be opened.
   * Use in controlled mode (see `open`).
   */
  onOpen?: () => void;
  value?: TDate;
  /**
   * The default visible view.
   * Used when the component view is not controlled.
   * Must be a valid option from `views` list.
   */
  openTo?: TView;
  /**
   * Maximal selectable date.
   * @example format - dayjs('MM-DD-YYYY') (or) dayjs(ISO) (or)
   * dayjs('Nov 10, 2023')
   */
  maxDate?: TDate;
  /**
   * Minimal selectable date.
   * @example format - dayjs('MM-DD-YYYY') (or) dayjs(ISO) (or)
   * dayjs('Nov 10, 2023')
   */
  minDate?: TDate;
  /**
   * Callback fired when the value changes.
   */
  onChange?: (
    value: TDate,
    context?: PickerChangeHandlerContext<DateValidationError>
  ) => void;
  /**
   * Format of the date when rendered in the input(s).
   * Defaults to localized format based on the used `views`.
   * @example DD-MM-YYYY = 30-11-2023
   *          ddd = Nov
   *          LL = February 22, 2022
   */
  format?: string;
  /**
   * Callback fired when the focuse was change.
   */
  onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  /**
   * Callback fired when the focuse was change.
   */
  onFocus?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
  /**
   * If true, the label is displayed in an error state
   * @default false
   */
  error?: boolean;
  /**
   * If `true`, the label is displayed as required and the `input` element is required.
   * @default false
   */
  required?: boolean;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
  /**
   * customize the input by css file
   */
  inputClassName?: string;
  /**
   * customize the lable by css file
   */
  inputLableStyle?: string;
  /**
   * Props applied to the Input element.
   * It will be a [`FilledInput`](/material-ui/api/filled-input/),
   * [`OutlinedInput`](/material-ui/api/outlined-input/) or [`Input`](/material-ui/api/input/)
   * component depending on the `variant` prop value.
   */
  InputProps?:
    | Partial<OutlinedInputProps>
    | Partial<InputProps>
    | Partial<FilledInputProps>;
  /**
   * If `true`, the label is shrunk.
   */
  shrink?: boolean;
  /**
   * The variant to use.
   * @default 'outlined'
   */
  variant?: "outlined" | "standard" | "filled";
  /**
   * The size of the component.
   * @default 'normal'
   */
  labelSize?: "small" | "normal";
}
