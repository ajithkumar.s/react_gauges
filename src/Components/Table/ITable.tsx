import { SxProps, Theme } from "@mui/system";
import {
  GridApiCommon,
  GridColDef,
  GridCsvGetRowsToExportParams,
  GridExportOptions,
  GridGetRowsToExportParams,
  GridLocaleText,
  GridPrintGetRowsToExportParams,
  GridRowId,
  GridRowsProp,
  GridValidRowModel,
} from "@mui/x-data-grid";
import { GridApiCommunity } from "@mui/x-data-grid/internals";

export type Columns = GridColDef[];
export type Rows = GridRowsProp | GridValidRowModel[];
export interface ColumnVisibilityModel {
  [key: string]: false;
}

export interface TableProps {
  rows: Rows;
  columns: Columns;
  /**
   * If `true`, the grid height is dynamic and follow the number of rows in the grid.
   * @default false
   */
  autoHeight?: boolean;
  /**
   * Set the density of the grid.
   * @default "standard"
   */
  density?: "compact" | "standard" | "comfortable";
  /**
   * If `true`, a  loading overlay is displayed.
   */
  loading?: boolean;
  /**
   * @example {id:false}
   */
  columnVisibilityModel?: ColumnVisibilityModel;
  /**
   * If `true`, the column menu is disabled.
   * @default false
   */
  disableColumnMenu?: boolean;
  /**
   * If `true`, the grid get a first column with a checkbox that allows to select rows.
   * @default false
   */
  checkboxSelection?: boolean;
  /**
   * To set the uniq key
   * @example (row)=> row.key
   * @default id
   */
  getRowId?: (T: ColumnVisibilityModel) => string | number;
  /**
   * Set the total number of rows, if it is different from the length of the value `rows` prop.
   * If some rows have children (for instance in the tree data), this number represents the amount of top level rows.
   */
  rowCount?: number;

  /**
   * If `true`, pagination is enabled.
   * @default false
   */
  pagination?: boolean;
  /**
   * Sets the height in pixel of a row in the grid.
   * @default 52
   */
  rowHeight?: number;
  /**
   * Select the pageSize dynamically using the component UI.
   * @default [25, 50, 100]
   */
  pageSizeOptions?: Array<
    | number
    | {
        value: number;
        label: string;
      }
  >;
  children?: React.ReactNode;
  /**
   * Set the number of rows in one page.
   * If some of the rows have children (for instance in the tree data), this number represents the amount of top level rows wanted on each page.
   * @default 25
   */
  initialPageSize?: number;
  localeText?: GridLocaleText;
  sx?: SxProps<Theme>;
}

export interface ICustomToolbar {
  children?: React.ReactNode;
  sx?: SxProps<Theme>;
  className?: string;
}
export interface ICustomFooter {
  children?: React.ReactNode;
  sx?: SxProps<Theme>;
  className?: string;
}

export interface IGridExportOptions {
  /**
   * The columns exported.
   * This should only be used if you want to restrict the columns exports.
   */
  fields?: string[];
  /**
   * If `true`, the hidden columns will also be exported.
   * @default false
   */
  allColumns?: boolean;
}

export interface ITablePrintProps extends IGridExportOptions {
  /**
   * The value to be used as the print window title.
   * @default The title of the page.
   */
  fileName?: string;
  /**
   * If `true`, the toolbar is removed for when printing.
   * @default false
   */
  hideToolbar?: boolean;
  /**
   * If `true`, the footer is removed for when printing.
   * @default false
   */
  hideFooter?: boolean;
  /**
   * If `true`, the selection checkboxes will be included when printing.
   * @default false
   */
  includeCheckboxes?: boolean;
  /**
   * If `false`, all <style> and <link type="stylesheet" /> tags from the <head> will not be copied
   * to the print window.
   * @default true
   */
  copyStyles?: boolean;
  /**
   * One or more classes passed to the print window.
   */
  bodyClassName?: string;
  className?: string;
  /**
   * Provide Print specific styles to the print window.
   */
  pageStyle?: string | Function;
  /**
   * Function that returns the list of row ids to export in the order they should be exported.
   * @param {GridPrintGetRowsToExportParams} params With all properties from [[GridPrintGetRowsToExportParams]].
   * @returns {GridRowId[]} The list of row ids to export.
   */
  getRowsToExport?: (params: GridPrintGetRowsToExportParams) => GridRowId[];
}

export interface GridFileExportOptions<
  Api extends GridApiCommon = GridApiCommunity
> extends GridExportOptions {
  /**
   * The string used as the file name.
   * @default `document.title`
   */
  fileName?: string;
  /**
   * If `true`, the first row of the file will include the headers of the grid.
   * @default true
   */
  includeHeaders?: boolean;
  /**
   * Function that returns the list of row ids to export on the order they should be exported.
   * @param {GridGetRowsToExportParams} params With all properties from [[GridGetRowsToExportParams]].
   * @returns {GridRowId[]} The list of row ids to export.
   */
  getRowsToExport?: (params: GridGetRowsToExportParams<Api>) => GridRowId[];
}

export interface ITableCSVExportProps extends GridFileExportOptions {
  /**
   * The character used to separate fields.
   * @default ','
   */
  delimiter?: string;
  /**
   * The string used as the file name.
   * @default `document.title`
   */
  fileName?: string;
  /**
   * If `true`, the UTF-8 Byte Order Mark (BOM) prefixes the exported file.
   * This can allow Excel to automatically detect file encoding as UTF-8.
   * @default false
   */
  utf8WithBom?: boolean;
  /**
   * If `true`, the CSV will include the column headers and column groups.
   * Use `includeColumnGroupsHeaders` to control whether the column groups are included.
   * @default true
   */
  includeHeaders?: boolean;
  /**
   * If `true`, the CSV will include the column groups.
   * @see See {@link https://mui.com/x/react-data-grid/column-groups/ column groups docs} for more details.
   * @default true
   */
  includeColumnGroupsHeaders?: boolean;
  /**
   * Function that returns the list of row ids to export on the order they should be exported.
   * @param {GridCsvGetRowsToExportParams} params With all properties from [[GridCsvGetRowsToExportParams]].
   * @returns {GridRowId[]} The list of row ids to export.
   */
  className?: string;
  getRowsToExport?: (params: GridCsvGetRowsToExportParams) => GridRowId[];
}