import "../Table.scss";

import { FunctionComponent } from "react";
import { TextInputFieldProps } from "../../TextInput/ITextInputFieldProps";
import { GridToolbarQuickFilter } from "@mui/x-data-grid/components/toolbar/GridToolbarQuickFilter";
import IconButtonField from "../../Button/IconButtonField";

const TableSearch: FunctionComponent<TextInputFieldProps> = (props) => {
  return (
    <GridToolbarQuickFilter
      className="table_search"
      variant="filled"
      InputProps={{
        disableUnderline: true,
      }}
      {...props}
    />
  );
};

export default TableSearch;
