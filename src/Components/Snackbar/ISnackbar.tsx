import { SnackbarProps } from "@mui/material";
import { IAPIResponse } from "../../Redux/interfaces/IAPIResponse";
import { IAlertProps } from "../Alert/IAlert";

// Create an interface for IAlertProps with omitted conflicting properties
interface CustomAlertProps
  extends Omit<
    IAlertProps,
    | "children"
    | "key"
    | "onClose"
    | "classes"
    | "color"
    | "ref"
    | "ReactNode"
    | "role"
  > {}

// New interface that combines both, resolving conflicts by omitting specific properties
export interface ISnackbarProps extends SnackbarProps, CustomAlertProps {
  response?: IAPIResponse;
}
