import { Dayjs } from "dayjs";
import { TextInputFieldProps } from "../TextInput/ITextInputFieldProps";

export interface ITimeRangePickers
  extends Omit<TextInputFieldProps, "onChange"> {
  /**
   * If `true`, the open picker button will not be rendered (renders only the field).
   * @default false
   */
  disableopenPickers?: boolean;
  /**
   * Format of the date when rendered in the input(s).
   * Defaults to localized format based on the used `views`.
   */
  format?: TimeFormat;
  /**
   * Choose which timezone to use for the value.
   * Example: "default", "system", "UTC", "America/New_York".
   * If you pass values from other timezones to some props, they will be converted to this timezone before being used.
   * @see See the {@link https://mui.com/x/react-date-pickers/timezone/ timezones documention} for more details.
   * @default The timezone of the `value` or `defaultValue` prop is defined, 'default' otherwise.
   */
  timezone?: PickersTimezone;
  /**
   * 12h/24h view for hour selection clock.
   * @default `utils.is12HourCycleInCurrentLocale()`
   */
  ampm?: boolean;
  /**
   * If `true`, disable values before the current date for date components, time for time components and both for date time components.
   * @default false
   */
  disablePast?: boolean;
  /**
   * If `true`, the popover or modal will close after submitting the full date.
   * @default `true` for desktop, `false` for mobile (based on the chosen wrapper and `desktopModeMediaQuery` prop).
   */
  closeOnSelect?: boolean;
  /**
   * If `true`, the picker and text field are disabled.
   * @default false
   */
  disabled?: boolean;
  /**
   * If `false`, the picker is not like text field.
   * @default true
   */
  isLikeTextField?: boolean;
  /**
   * The selected value.
   * Used when the component is controlled.
   */
  valueTimeRangePicker?: ITimeRange;
  /**
   * Custom Error Message when your using react hook forms and zod validation
   *
   */
  customErrorMessage?: string;
  restrictEndTime?: boolean;
  firstPickerLabel?: string;
  secondPickerLabel?: string;
  onChange?: (val: ITimeRange) => void;
  clicOk?: () => void;
}
export type TimeT = Date | Dayjs | string | null;
type TimeFormat = "HH:mm:ss" | "hh:mm:ss A" | "HH:mm:ssZ" | "hh:mm A";
export type PickersTimezone =
  | "default"
  | "system"
  | "UTC"
  | "Asia/Kolkata"
  | string;
export interface ITimeRange {
  startTime: TimeT;
  endTime: TimeT;
}
