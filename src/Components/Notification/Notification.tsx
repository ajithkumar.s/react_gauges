import React, { FunctionComponent, ReactElement } from "react";
import { NotificationProps } from "./INotification";

import { Box, Divider, List, ListItem, Popover } from "@mui/material";

const Notifications: FunctionComponent<NotificationProps> = (props) => {
  const {
    autoFocus,
    classes,
    sx,
    transformOrigin = {
      vertical: "top",
      horizontal: "right",
    },
    anchorOrigin = {
      vertical: "bottom",
      horizontal: "center",
    },
    notificationItems,
    notificationHeader,
    children,
  } = props;

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      {React.Children.map(children, (child) => {
        if (React.isValidElement(child)) {
          // Combine original children onclick with component onclick
          const originalOnClick = (child as ReactElement).props.onClick;
          const enhancedOnClick = (event: React.MouseEvent<HTMLElement>) => {
            if (originalOnClick) {
              originalOnClick(event); // children onclick
            }
            handleClick(event); // component onclick
          };
          return React.cloneElement(child as ReactElement, {
            onClick: enhancedOnClick,
          });
        }
        return child;
      })}
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        transformOrigin={transformOrigin}
        anchorOrigin={anchorOrigin}
        autoFocus={autoFocus}
        sx={sx}
        classes={classes}
      >
        <Box>
          {notificationHeader && <header>{notificationHeader}</header>}
          <div style={{ overflowY: "scroll", maxHeight: "300px" }}>
            <List>
              {notificationItems?.map((item, index) => (
                <>
                  <ListItem key={index}>{item.content}</ListItem>
                  <Divider />
                </>
              ))}
            </List>
          </div>
        </Box>
      </Popover>
    </>
  );
};

export default Notifications;
