import { createSlice } from "@reduxjs/toolkit";
import { IReduxState } from "../../interfaces/IReduxState";
import { UserPermissionRowsType } from "./IUserPermission";

export const UserPermissionRows = createSlice({
  name: "UserPermissionRows",
  initialState: {
    data: {},
    isLoading: false,
  } as IReduxState<UserPermissionRowsType[]>,
  reducers: {
    userPermissionRowsList: (state, _) => {
      state.isLoading = true;
    },
    successUserPermissionRows: (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
    },
    failureUserPermissionRows: (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
    },
  },
});

export const {
  userPermissionRowsList,
  successUserPermissionRows,
  failureUserPermissionRows,
} = UserPermissionRows.actions;
export default UserPermissionRows.reducer;
