import { createSlice } from "@reduxjs/toolkit";
import { IReduxState } from "../../interfaces/IReduxState";
import { UserPermissionType } from "./IUserPermission";

export const UserPermissionRoles = createSlice({
  name: "UserPermissionRoles",
  initialState: {
    data: {},
    isLoading: false,
  } as IReduxState<UserPermissionType[]>,
  reducers: {
    userPermissionList: (state) => {
      state.isLoading = true;
    },
    successUserPermission: (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
    },
    failureUserPermission: (state, action) => {
      state.data = action.payload;
      state.isLoading = false;
    },
  },
});

export const {
  userPermissionList,
  successUserPermission,
  failureUserPermission,
} = UserPermissionRoles.actions;
export default UserPermissionRoles.reducer;
