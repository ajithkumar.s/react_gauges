import { PayloadAction } from "@reduxjs/toolkit";
import api from "../../Axios/middleware";
import { AxiosResponse } from "axios";
import { call, put } from "redux-saga/effects";

import {
  failureUserPermission,
  successUserPermission,
} from "./UserPermissionRolesSlicer";

import {
  successUserPermissionRows,
  failureUserPermissionRows,
} from "./UserPermissionRowsSlicer";
// get user permission
const UserPermissionApi = () => {
  return api.get("/v1/api/role/?offset=0&limit=100");
};

export function* ListUserPermissionCall() {
  try {
    const response: AxiosResponse = yield call(UserPermissionApi);
    yield put(successUserPermission(response.data));
  } catch (error) {
    yield put(failureUserPermission(error));
  }
}

// get User permission rows
const UserPermissionRowsApi = (action?: PayloadAction<number>) => {
  const id = action?.payload;
  return api.get(`/v1/api/user_permission/`, {
    params: {
      ...(id ? { roleId: id } : {}),
    },
  });
};

export function* ListUserPermissionRowsCall(action: PayloadAction<number>) {
  try {
    const response: AxiosResponse = yield call(UserPermissionRowsApi, action);
    yield put(successUserPermissionRows(response.data));
  } catch (error) {
    yield put(failureUserPermissionRows(error));
  }
}
