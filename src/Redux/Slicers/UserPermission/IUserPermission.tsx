export type UserPermissionType = {
  created_by: number;
  name: string;
  updated_by: number;
  updated_on: Date;
  active: boolean;
  id: number;
  created_on: Date;
};

export type UserPermissionRowsType = {
  id: number;
  menuname: string;
  roleId?: number | string | null;
  active: boolean;
  add: boolean;
  edit: boolean;
  view: boolean;
  delete: boolean;
};
