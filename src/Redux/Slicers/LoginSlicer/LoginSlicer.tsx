import { createSlice } from "@reduxjs/toolkit";
import { IReduxState } from "../../interfaces/IReduxState";

export const LoginSlicer = createSlice({
  name: "LoginSlicer",
  initialState: {
    data: {},
    isLoading: false,
  } as IReduxState<null>,
  reducers: {
    loginStart: (state, _) => {
      state.isLoading = true;
    },
    successLogin: (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
    },
    failureLogin: (state, action) => {
      state.data = action.payload;
      state.isLoading = false;
    },
  },
});

export const { loginStart, successLogin, failureLogin } = LoginSlicer.actions;

export default LoginSlicer.reducer;
