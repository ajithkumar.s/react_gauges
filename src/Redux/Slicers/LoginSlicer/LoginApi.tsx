import { PayloadAction } from "@reduxjs/toolkit";
import api from "../../Axios/middleware";
import { AxiosResponse } from "axios";
import { call, put } from "redux-saga/effects";
import { successLogin, failureLogin } from "./LoginSlicer";
const LoginApi = (action: PayloadAction<{}>) => {
  return api.post("/v1/api/user/login", action.payload);
};

export function* LoginCall(action: PayloadAction<{}>) {
  try {
    const respons: AxiosResponse = yield call(LoginApi, action);
    yield put(successLogin(respons.data));
    if (respons.data.data) {
      localStorage.setItem("token", respons.data.data.token);
    }
  } catch (error) {
    yield put(failureLogin(error));
  }
}
