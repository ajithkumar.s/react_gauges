import {configureStore} from "@reduxjs/toolkit"
import createSagaMiddleware from "redux-saga"
import rootSage from "../Saga/Saga"
import TestReducer from "../Slicers/TestSlicers/TestSlicers"
import SessionExpiredReducer from "../Slicers/SessionExpired/SessionExpired"
import UserPermissionRolesReducer from "../Slicers/UserPermission/UserPermissionRolesSlicer"
import UserPermissionRowsReducer from "../Slicers/UserPermission/UserPermissionRowsSlicer";
import LoginReducer from "../Slicers/LoginSlicer/LoginSlicer"
const sagaMiddleware = createSagaMiddleware()

export const store = configureStore({
    reducer:{
        Test: TestReducer,
        // SessionExpired
        SessionExpired:SessionExpiredReducer,
        // User Permission
        UserPermissionRoles:UserPermissionRolesReducer,
        UserPermissionRows: UserPermissionRowsReducer,
        // Login
        LoginSlicer:LoginReducer

    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
})

sagaMiddleware.run(rootSage)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;