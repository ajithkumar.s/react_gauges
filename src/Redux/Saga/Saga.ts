import {takeEvery,takeLatest} from "redux-saga/effects"
import { TestCall } from "../Slicers/TestSlicers/TestApi"
import {ListUserPermissionCall} from "../Slicers/UserPermission/UserPermissionAPI"
import {ListUserPermissionRowsCall} from "../Slicers/UserPermission/UserPermissionAPI"
import {LoginCall} from "../Slicers/LoginSlicer/LoginApi"

export default function* rootSage(){
    yield takeLatest("Test/testCall",TestCall)
    // yield takeLatest("pattern",worker)
    // User Permission
    yield takeLatest("UserPermissionRoles/userPermissionList", ListUserPermissionCall)
    yield takeLatest("UserPermissionRows/userPermissionRowsList",ListUserPermissionRowsCall)
    yield takeLatest("LoginSlicer/loginStart",LoginCall)
}