import { createBrowserRouter } from "react-router-dom";
import ErrorPage from "../Pages/ErrorPage/ErrorPage";
import App from "../App";

import PrivateRoute from "./PrivateRoute";
import Login from "../Pages/Login/Login";
import NotAuthorised from "../Pages/NotAuthorised/NotAuthorised";
import UserPermissions from "../Pages/UserPermission/UserPermissions";
import Dashboard from "../Pages/Dashboard/Dashboard";
import Reports from "../Pages/Reports/Reports";
import Setting from "../Pages/Settings/Setting";
import Transaction from "../Pages/Transactions/Transaction";
import ForgotPassword from "../Pages/ForgotPassword/ForgotPassword";
import Product from "../Pages/Product/Product";
import TestArea from "../Pages/Demo/TestArea";
import MQTTChart from "../Pages/DemoImplementation/MQTTChart";
import ReactGauges from "../Pages/DemoImplementation/Guages/ReactGauges";

export const router = createBrowserRouter([
  { path: "/login", element: <Login /> },
  { path: "/forgotPassword", element: <ForgotPassword /> },
  {
    path: "/",
    errorElement: <ErrorPage />,
    // loader:,
    element: <PrivateRoute element={<App />} />,
    children: [
      {
        path: "dashboard",
        element: <MQTTChart />,
      },
      {
        path: "userPermission",
        element: <UserPermissions />,
      },
      {
        path: "product",
        children: [
          {
            path: "sub",
            children: [
              {
                index: true,
                element: <Product />,
              },
            ],
          },
          {
            path: "fun",
            element: <TestArea />,
          },
        ],
      },
      {
        path: "NotAuthorised",
        element: <NotAuthorised />,
      },
      {
        path: "reports",
        element: <Reports />,
      },
      {
        path: "setting",
        element: <Setting />,
      },
      {
        path: "transactions",
        element: <Transaction />,
      },
    ],
  },
]);
