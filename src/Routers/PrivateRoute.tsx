import { FunctionComponent } from "react";
import { Navigate, useLocation, useParams } from "react-router-dom";
import { menu, skipedMenu } from "../Components/Sidnavbar/menuData";
import { IMenuItem } from "../Components/Sidnavbar/ISidnavbar";

interface PrivateRouteProps {
  element: React.ReactNode;
}

//    Must be outside the component
const isAutendicated = () => {
  return localStorage.getItem("token");
};

// Function to recursively flatten nested menu items
const flattenMenu = (menu: { [key: string]: IMenuItem[] }): IMenuItem[] => {
  const flatItems: IMenuItem[] = [];

  const recurse = (items: IMenuItem[]) => {
    items.forEach((item) => {
      flatItems.push(item);
      if (item.items) {
        recurse(item.items);
      }
    });
  };

  Object.values(menu).forEach((section) => recurse(section));
  return flatItems;
};

// Flatten the menu
const flattenedMenu = flattenMenu(menu);

// Function to check if a location has view permission
const hasViewPermission = (locationName: string): boolean | undefined => {
  const menuItem = flattenedMenu.find((item) => `/${item.to}` === locationName);
  return menuItem?.permissions?.view;
};
//

const PrivateRoute: FunctionComponent<PrivateRouteProps> = ({ element }) => {
  const location = useLocation();
  const params = useParams();
  const locationName = Object.keys(params).length
    ? location.pathname.replace(/\/[^/]+$/, "")
    : location.pathname;

  console.log(locationName);
  console.log(hasViewPermission(locationName));

  if (locationName === "/NotAuthorised") {
    return <>{element}</>;
  }

  if (isAutendicated()) {
    if (hasViewPermission(locationName) || skipedMenu.includes(locationName)) {
      return <>{element}</>;
    } else {
      return <Navigate to={"/NotAuthorised"} replace />;
    }
  } else {
    return <Navigate to={"/login"} replace />;
  }
};

export default PrivateRoute;
