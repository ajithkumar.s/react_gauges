import React from "react";
import "../Login/Login.scss";
import TextInputField from "../../Components/TextInput/TextInputField";
import { Divider, InputAdornment, SvgIcon } from "@mui/material";
import { Mail } from "@mui/icons-material";
import SendIcon from "@mui/icons-material/Send";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ButtonField from "../../Components/Button/ButtonField";
import { Link } from "react-router-dom";

import { useFormik } from "formik";
import * as Yup from "yup";

interface ForgotProps {
  userName: string;
  email: string;
}
const ForgotPassword = () => {
  const forgotInitialValues: ForgotProps = {
    userName: "",
    email: "",
  };

  const forgotSchema = Yup.object({
    userName: Yup.string().required("User Name is required"),
    email: Yup.string().email("Invalid email").required("Email is required"),
  });

  const forgotForm = useFormik({
    initialValues: forgotInitialValues,
    validationSchema: forgotSchema,
    onSubmit: (value, { resetForm }) => {
      console.log(value);
      resetForm();
    },
  });
  return (
    <div className="backGround">
      <form onSubmit={forgotForm.handleSubmit}>
        <div className="main">
          <div>
            <div className="heading">Forgot Password</div>
            <Divider />
          </div>

          <div className="textFields">
            <TextInputField
              className="textField"
              type="text"
              name="userName"
              value={forgotForm.values.userName}
              onChange={forgotForm.handleChange}
              onBlur={forgotForm.handleBlur}
              helperText={
                forgotForm.touched.userName && forgotForm.errors.userName
              }
              error={
                forgotForm.touched.userName &&
                Boolean(forgotForm.errors.userName)
              }
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      sx={{
                        width: "28px",
                        height: "28px",
                      }}
                      component={AccountCircleIcon}
                      inheritViewBox
                    />
                  </InputAdornment>
                ),
              }}
              placeholder="User Name"
            />
            <TextInputField
              className="textField"
              type="email"
              name="email"
              value={forgotForm.values.email}
              onChange={forgotForm.handleChange}
              onBlur={forgotForm.handleBlur}
              helperText={forgotForm.touched.email && forgotForm.errors.email}
              error={
                forgotForm.touched.email && Boolean(forgotForm.errors.email)
              }
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      sx={{
                        width: "23px",
                        height: "23px",
                      }}
                      component={Mail}
                      inheritViewBox
                    />
                  </InputAdornment>
                ),
              }}
              placeholder="Email"
            />
          </div>
          <div className="textFields">
            <ButtonField className="buttonfield">
              Send Password{" "}
              <SendIcon
                sx={{ marginLeft: "10px", width: "20px", height: "20px" }}
                component={SendIcon}
                inheritViewBox
              />
            </ButtonField>
            <Link className="login_page" to={"/login"}>
              <ButtonField className="buttonfield">Login</ButtonField>
            </Link>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ForgotPassword;
