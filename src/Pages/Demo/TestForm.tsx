import React, {
  FormEvent,
  FunctionComponent,
  useEffect,
  useState,
} from "react";
import TextInputField from "../../Components/TextInput/TextInputField";
import ButtonField from "../../Components/Button/ButtonField";
import { FormikProps, useFormik } from "formik";
import * as Yup from "yup";
import { ILoginForm } from "../Login/ILogin";
import { Divider, InputAdornment, SvgIcon } from "@mui/material";

interface TestFormProps {
  loginForm: (data: FormikProps<ILoginForm>) => void;
}

const TestForm: FunctionComponent = () => {
  const loginInitialValues: ILoginForm = {
    email: "",
    password: "",
  };

  const loginSchema = Yup.object({
    email: Yup.string()
      .email("Invalid Email")
      .required("User Name is required"),
    password: Yup.string().required("Password is required"),
  });

  const loginForm = useFormik({
    initialValues: loginInitialValues,
    validationSchema: loginSchema,
    onSubmit: async (value) => {
      console.log(value);
    },
  });
  const [formData, setFormData] = useState({
    name: "",
    email: "",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    console.log("form Data :", formData);
  };
  useEffect(() => {}, []);

  return (
    <>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div className="main">
          <div>
            <div className="heading">Login</div>
            {/* <div className="subHeading">
                Don't have an account yet? <Link to={"#"}>Sign Up</Link>
              </div> */}
            <Divider />
          </div>

          <div className="textFields">
            <TextInputField
              className="textField"
              type="text"
              id="name"
              name="name"
              required={true}
              onChange={handleChange}
              // value={loginForm.values.email}
              // onChange={loginForm.handleChange}
              // onBlur={loginForm.handleBlur}
              // placeholder="User Name"
              // helperText={loginForm.touched.email && loginForm.errors.email}
              // error={loginForm.touched.email && Boolean(loginForm.errors.email)}
            />

            <TextInputField
              className="textField"
              id="email"
              name="email"
              onChange={handleChange}
              // value={loginForm.values.password}
              // onChange={loginForm.handleChange}
              // onBlur={loginForm.handleBlur}
              // helperText={
              //   loginForm.touched.password && loginForm.errors.password
              // }
              // error={
              //   loginForm.touched.password && Boolean(loginForm.errors.password)
              // }
              // placeholder="Password"
              required={true}
            />
          </div>

          <div className="textFields">
            <ButtonField className="buttonfield" type="submit">
              Save
            </ButtonField>
          </div>
        </div>
      </form>
    </>
  );
};

export default TestForm;
