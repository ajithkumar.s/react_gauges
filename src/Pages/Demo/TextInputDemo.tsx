import React, { FunctionComponent, useEffect, useState } from "react";
import TextInputField from "../../Components/TextInput/TextInputField";
import { FormikProps, useFormik } from "formik";
import * as Yup from "yup";
import UploadButtonField from "../../Components/Button/UploadButtonField";
import { TextInputFieldProps } from "../../Components/TextInput/ITextInputFieldProps";
type RegexT = "email" | "password";

interface TextInputProps extends TextInputFieldProps {
  id?: string;
  name?: string;
  label?: string;
  regex?: RegexT;
  value?: string;
  onValueChange?: (value: string) => void;
}

const TextInputDemo: FunctionComponent<TextInputProps> = (props) => {
  const { label, id, name, onValueChange, regex, value, ...rest } = props;
  const [inputValue, setInputValue] = useState<string>("");

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const handleBlur = () => {
    if (inputValue === "") {
      setErrorMessage(`${label} is required`);
    }
  };
  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const newValue = e.target.value;
    if (regex) {
      switch (regex) {
        case "email":
          const emailRegex =
            /^(?=.{1,256}$)[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
          if (!emailRegex.test(newValue)) {
            setErrorMessage(`Invalid ${label}`);
            return;
          }
          break;
        case "password":
          const passwordRegex =
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
          if (!passwordRegex.test(newValue)) {
            setErrorMessage(`Invalid ${label}`);
            return;
          }
          break;
        default:
          break;
      }
    }
    // Clear error message if input is valid
    setErrorMessage(null);
    setInputValue(newValue);
  };
  useEffect(() => {
    if (inputValue && onValueChange) onValueChange(inputValue);
  }, [inputValue]);

  return (
    <>
      <TextInputField
        {...rest}
        label={label}
        id={id}
        name={name}
        value={value}
        onChange={(e) => handleChange(e)}
        onBlur={handleBlur}
        error={Boolean(errorMessage)}
        helperText={errorMessage}
      />
    </>
  );
};

export default TextInputDemo;
