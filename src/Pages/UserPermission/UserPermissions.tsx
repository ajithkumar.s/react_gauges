import React, { useEffect } from "react";
import UserPermission from "../../Components/UserPermission/UserPermission";
import { Rows } from "../../Components/Table/ITable";
import "./UserPermissions.scss";

import { useAppDispatch, useAppSelector } from "../../Redux/Store/Hooks";
import { userPermissionList } from "../../Redux/Slicers/UserPermission/UserPermissionRolesSlicer";
import { userPermissionRowsList } from "../../Redux/Slicers/UserPermission/UserPermissionRowsSlicer";
import Snackbar from "../../Components/Snackbar/Snackbar";

const UserPermissions: React.FC = () => {
  const dispatch = useAppDispatch();
  const permissionData = useAppSelector((state) => state.UserPermissionRoles);
  const userPermissionRowsData = useAppSelector(
    (state) => state.UserPermissionRows
  );
  useEffect(() => {
    dispatch(userPermissionList());
    dispatch(userPermissionRowsList(null));
  }, []);

  // console.log(permissionData.data);
  // console.log(userPermissionRowsData.data?.data);

  const filteredRole = permissionData.data?.data?.map((role) => ({
    value: role.id,
    label: role.name,
  }));
  console.log(userPermissionRowsData.data);

  return (
    <div className="user_permission_container">
      <Snackbar response={userPermissionRowsData.data} />
      <UserPermission
        rows={userPermissionRowsData.data?.data ?? []}
        RoleOptions={filteredRole ?? []}
        getRoleValues={(val) => console.log(val)}
        getRoleOption={(val) => {
          console.log(val?.value);
          dispatch(userPermissionRowsList(val?.value));
        }}
      />
    </div>
  );
};

export default UserPermissions;
