import React, { useEffect, useRef, useState } from "react";
import GaugeChart from "react-gauge-chart";
import "./ReactGauges.scss";
import ReactSpeedometer, {
  CustomSegmentLabelPosition,
  Transition,
} from "react-d3-speedometer";
import GaugeComponent from "react-gauge-component";

const useInterval = (callback: () => void, delay: number) => {
  const savedCallback = useRef<() => void>();

  // Remember the latest callback if it changes.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current();
      }
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};
const ReactGauges = () => {
  const [value, setValue] = useState(0.1);
  const [rndValue, setRndValue] = useState<number>();
  const valueRef = useRef(value);
  valueRef.current = value;

  console.log(value);
  useInterval(() => {
    const randomNumber = (): void => {
      let random = Math.random();

      // Apply a mathematical function to scale the value to the range [0.1, 1]
      let scaledValue =
        0.1 +
        0.9 * Math.abs(Math.sin(random * Math.PI * 2) * Math.log(random + 1));

      // Ensure the result is within the [0.1, 1] range
      if (scaledValue < 0.1) scaledValue = 0.1;
      if (scaledValue > 1) scaledValue = 1;

      setValue(scaledValue);
    };

    randomNumber();
  }, 1000);

  useInterval(() => {
    const randomNumber = (): void => {
      let random = Math.random();

      let scaledValue =
        1 +
        9999 * Math.abs(Math.sin(random * Math.PI * 2) * Math.log(random + 1));

      // Ensure the result is within the [1, 10000] range
      if (scaledValue < 1) scaledValue = 1;
      if (scaledValue > 10000) scaledValue = 10000;
      let finalValue = Math.round(scaledValue);
      setRndValue(finalValue);
    };

    randomNumber();
  }, 1000);
  function segmentValueFormatter(value: any) {
    console.log(value);

    if (value < 1000) {
      return `${0} %`;
    }
    if (value < 2000) {
      return `${1000}`;
    }
    if (value < 3000) {
      return `${2000} `;
    }
    if (value < 4000) {
      return `${3000}`;
    }
    if (value < 5000) {
      return `${4000} `;
    }
    if (value < 6000) {
      return `${5000} `;
    }
    if (value < 7000) {
      return `${6000} `;
    }
    if (value < 8000) {
      return `${7000} `;
    }
    if (value < 9000) {
      return `${8000} `;
    }
    if (value < 10000) {
      return `${9000} `;
    }

    return `${10000}`;
  }
  return (
    <>
      <div className="gauge_container">
        <div className="speedo_gauge">
          <ReactSpeedometer
            // fluidWidth={true}
            forceRender={false}
            value={rndValue}
            segmentValueFormatter={segmentValueFormatter}
            maxValue={10000}
            segments={10}
            needleTransitionDuration={500}
            needleTransition={Transition.easeLinear}
            paddingHorizontal={15}
            paddingVertical={15}
            labelFontSize={"10px"}
            valueTextFontSize={"23px"}
            // height={400}
            // width={600}
          />
        </div>
        <ReactSpeedometer
          width={400}
          needleHeightRatio={0.7}
          value={rndValue}
          currentValueText={`Air Quality: ${rndValue}µg/m³`}
          maxValue={10000}
          segmentValueFormatter={segmentValueFormatter}
          customSegmentLabels={[
            {
              text: "Very Bad",
              color: "#555",
              position: CustomSegmentLabelPosition.Outside,
            },
            {
              text: "Bad",
              color: "#555",
            },
            {
              text: "Ok",
              color: "#555",
              fontSize: "19px",
            },
            {
              text: "Good",
              color: "#555",
            },
            {
              text: "Very Good",
              color: "#555",
            },
          ]}
          ringWidth={67}
          needleTransitionDuration={333}
          needleColor={"#90f2ff"}
          textColor={"#00000"}
        />
        <ReactSpeedometer
          needleHeightRatio={0.5}
          maxSegmentLabels={5}
          segments={2}
          customSegmentStops={[0, 250, 500, 750, 1500, 1000]}
          segmentColors={["firebrick", "tomato", "gold", "limegreen"]}
          value={1333}
          textColor={"#00000"}
        />
        <ReactSpeedometer
          needleHeightRatio={0.7}
          maxSegmentLabels={5}
          segments={555}
          value={333}
          textColor={"#00000"}
          ringWidth={12}
        />
        <GaugeComponent
          arc={{
            subArcs: [
              {
                limit: 20,
                color: "#EA4228",
                showTick: true,
                tooltip: { text: "Empty" },
              },
              {
                limit: 40,
                color: "#F58B19",
                showTick: true,
                tooltip: { text: "Low" },
              },
              {
                limit: 60,
                color: "#F5CD19",
                showTick: true,
                tooltip: { text: "Fine" },
              },
              {
                limit: 100,
                color: "#5BE12C",
                showTick: true,
                tooltip: { text: "Full" },
              },
            ],
          }}
          value={value * 100}
        />
        <GaugeComponent
          id="gauge-component4"
          type="semicircle"
          arc={{
            gradient: true,
            width: 0.15,
            padding: 0,
            subArcs: [
              {
                limit: 5,
                color: "#EA4228",
              },
              {
                limit: 20,
                color: "#F5CD19",
              },
              {
                limit: 58,
                color: "#5BE12C",
              },
              {
                limit: 75,
                color: "#F5CD19",
              },
              { color: "#EA4228" },
            ],
          }}
          labels={{
            tickLabels: {
              type: "outer",
              ticks: [
                { value: 0 },
                { value: 20 },
                { value: 40 },
                { value: 60 },
                { value: 80 },
              ],
            },
          }}
          value={value * 100}
          pointer={{ type: "arrow", color: "#dfa810" }}
        />
        <GaugeComponent
          type="semicircle"
          arc={{
            width: 0.2,
            padding: 0.005,
            cornerRadius: 1,
            // gradient: true,
            subArcs: [
              {
                limit: 30,
                color: "#EA4228",
                showTick: true,
                tooltip: {
                  text: "Too low temperature!",
                },
                onClick: () => console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"),
                onMouseMove: () =>
                  console.log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"),
                onMouseLeave: () =>
                  console.log("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"),
              },
              {
                limit: 50,
                color: "#F5CD19",
                showTick: true,
                tooltip: {
                  text: "Low temperature!",
                },
              },
              {
                limit: 80,
                color: "#5BE12C",
                showTick: true,
                tooltip: {
                  text: "OK temperature!",
                },
              },
              {
                limit: 90,
                color: "#F5CD19",
                showTick: true,
                tooltip: {
                  text: "High temperature!",
                },
              },
              {
                color: "#EA4228",
                tooltip: {
                  text: "Too high temperature!",
                },
              },
            ],
          }}
          pointer={{
            color: "#345243",
            length: 0.8,
            width: 15,
            elastic: false,
            animationDelay: 0,
          }}
          labels={{
            valueLabel: { formatTextValue: (value) => value + "ºC" },
            tickLabels: {
              type: "outer",
              defaultTickValueConfig: {
                formatTextValue: (value: any) => value + "ºC",
              },
              //   valueConfig: {
              //     formatTextValue: (value:any) => value + "ºC",
              //     fontSize: 10,
              //   },
              //   ticks: [{ value: 13 }, { value: 22.5 }, { value: 32 }],
            },
          }}
          value={value * 100}
          minValue={10}
          maxValue={100}
        />
        <GaugeComponent
          type="semicircle"
          arc={{
            colorArray: ["#00FF15", "#FF2121"],
            padding: 0.02,
            subArcs: [
              { limit: 40 },
              { limit: 60 },
              { limit: 70 },
              {},
              {},
              {},
              {},
            ],
          }}
          pointer={{ type: "blob", animationDelay: 0 }}
          value={value * 100}
        />
        <GaugeComponent
          value={value * 100}
          type="radial"
          labels={{
            tickLabels: {
              type: "outer",
              ticks: [
                { value: 20 },
                { value: 40 },
                { value: 60 },
                { value: 80 },
                { value: 100 },
              ],
            },
          }}
          arc={{
            colorArray: ["#5BE12C", "#EA4228"],
            subArcs: [{ limit: 10 }, { limit: 30 }, {}, {}, {}],
            padding: 0.02,
            width: 0.3,
          }}
          pointer={{
            elastic: false,
            animationDelay: 0,
          }}
        />
        <div className="demo_check">
          <GaugeComponent
            value={value * 100}
            type="radial"
            labels={{
              tickLabels: {
                type: "outer",
                ticks: [
                  { value: 20 },
                  { value: 40 },
                  { value: 60 },
                  { value: 80 },
                  { value: 100 },
                ],
              },
            }}
            arc={{
              colorArray: ["#5BE12C", "#EA4228"],
              nbSubArcs: 70,
              padding: 0.02,
              width: 0.3,
            }}
            pointer={{
              elastic: false,
              animationDelay: 0,
            }}
          />
          <h1>Humidity</h1>
        </div>
        <GaugeComponent
          id="gauge-component-radial"
          value={value * 100}
          type="radial"
          arc={{
            width: 0.2,
            nbSubArcs: 20,
            colorArray: ["#FF5F6D", "#FFC371"],
          }}
          labels={{
            tickLabels: {
              type: "inner",
              ticks: [
                { value: 20 },
                { value: 40 },
                { value: 60 },
                { value: 80 },
                { value: 100 },
              ],
            },
          }}
          pointer={{
            length: 0.5,
            animate: false,
            animationDelay: 0,
            animationDuration: 0,
          }}
        />
        <GaugeComponent
          id="gauge-component-radial"
          value={value * 100}
          type="radial"
          arc={{
            width: 0.2,
            cornerRadius: 10,
            colorArray: ["#FF5F6D", "#FFC371"],
            gradient: true,
          }}
          labels={{
            valueLabel: {
              matchColorWithArc: true,
              formatTextValue: (val) => {
                return `Temp ${val}%`;
              },
              maxDecimalDigits: 1,
            },
            tickLabels: {
              type: "inner",
              ticks: [
                { value: 20 },
                { value: 40 },
                { value: 60 },
                { value: 80 },
                { value: 100 },
              ],
            },
          }}
          pointer={{
            length: 0.5,
            animate: false,
            animationDelay: 0,
            animationDuration: 0,
          }}
        />
      </div>
    </>
  );
};

export default ReactGauges;
