import React, { useEffect, useState, useCallback } from "react";
import mqtt, { IClientOptions } from "mqtt";
import { Line } from "react-chartjs-2";
import "chart.js/auto";
import moment, { duration } from "moment";
import "./MqttChart.scss";
import {
  ChartConfiguration,
  ChartData as IChartDataType,
  ChartOptions,
  ChartDataset,
} from "chart.js";

export interface ChartData {
  labels: string[];
  datasets: ChartDataset<"line">[];
}

export interface IdataArray {
  humData: number[];
  humLabelData: string[];
}

const MQTTChart = () => {
  const newDate = new Date();
  const [stop, setStop] = useState<boolean>(false);
  const [centHumidityData, setCentHumidityData] = useState<ChartData>({
    labels: [],
    datasets: [
      {
        label: `Humidity on ${moment(newDate).format("DD/MM/YYYY")} `,
        data: [],
        fill: true,
        borderColor: "orangered",
        backgroundColor: (cntx) => {
          const bgColor = [
            "rgba(255, 26, 104, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(0, 0, 0, 0.2)",
          ];
          if (!cntx.chart.chartArea) {
            return;
          }
          console.log(cntx.chart.chartArea);
          const {
            ctx,
            data,
            chartArea: { top, bottom },
          } = cntx.chart;
          const gradientBg = ctx.createLinearGradient(0, top, 0, bottom);
          gradientBg.addColorStop(0, bgColor[0]);
          gradientBg.addColorStop(0.5, bgColor[1]);
          gradientBg.addColorStop(1, bgColor[2]);
          return gradientBg;
        },
        borderWidth: 1.5,
        tension: 0.4,
        pointRadius: 3.0,
        pointHoverRadius: 3.5,
      },
    ],
  });
  const [humDataArray, setHumDataArray] = useState<IdataArray[]>([]);
  const [tempDataArray, setTempDataArray] = useState<IdataArray[]>([]);

  const [data, setData] = useState<ChartData>({
    labels: [],
    datasets: [
      {
        label: `Temperature on ${moment(newDate).format("DD/MM/YYYY")} `,
        data: [],
        fill: true,
        borderColor: "orangered",
        backgroundColor: (cntx) => {
          const bgColor = [
            "rgba(234, 51, 51, 0.37)",
            "rgba(234, 51, 51, 0.32)",
            "rgba(234, 51, 51, 0.27)",
            "rgba(234, 51, 51, 0.21)",
            "rgba(234, 51, 51, 0.16)",
            "rgba(234, 51, 51, 0.1)",
            "rgba(234, 51, 51, 0.06)",
          ];
          if (!cntx.chart.chartArea) {
            return;
          }
          console.log(cntx.chart.chartArea);
          const {
            ctx,
            data,
            chartArea: { top, bottom },
          } = cntx.chart;
          const gradientBg = ctx.createLinearGradient(0, top, 0, bottom);
          const colorTranches = 1 / (bgColor.length - 1);
          for (let i = 0; i < bgColor.length; i++) {
            gradientBg.addColorStop(0 + i * colorTranches, bgColor[i]);
          }

          return gradientBg;
        },
        borderWidth: 1.5,
        tension: 0.4,
        pointRadius: 0.5,
        pointHoverRadius: 0.5,
      },
    ],
  });

  const [tempData, setTempData] = useState<ChartData>({
    labels: [],
    datasets: [
      {
        label: `Temperature on ${moment(newDate).format("DD/MM/YYYY")} `,
        data: [],
        fill: true,
        backgroundColor: (cntx) => {
          const bgColor = [
            "rgba(255, 26, 104, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(0, 0, 0, 0.2)",
          ];
          if (!cntx.chart.chartArea) {
            return;
          }
          console.log(cntx.chart.chartArea);
          const {
            ctx,
            data,
            chartArea: { top, bottom },
          } = cntx.chart;
          const gradientBg = ctx.createLinearGradient(0, top, 0, bottom);
          const colorTranches = 1 / bgColor.length - 1;
          for (let i = 0; i < bgColor.length; i++) {
            gradientBg.addColorStop(0 + i * colorTranches, bgColor[i]);
            gradientBg.addColorStop(0.5, bgColor[1]);
            gradientBg.addColorStop(1, bgColor[2]);
          }

          return gradientBg;
        },
        borderColor: "#FF76CE",
      },
    ],
  });

  const processData = useCallback(
    (
      chartData: { datasets: { data: any }[]; labels: any },
      setChartData: (arg0: (prevData: any) => any) => void,
      dataArray: any,
      setDataArray: (arg0: (prevArray: any) => any[]) => void
    ) => {
      if (chartData.datasets[0].data.length >= 100) {
        const centData = {
          humData: chartData.datasets[0].data,
          humLabelData: chartData.labels,
        };
        setDataArray((prevArray) => [...prevArray, centData]);

        setChartData((prevData) => ({
          ...prevData,
          labels: [],
          datasets: [{ ...prevData.datasets[0], data: [] }],
        }));
      }
    },
    []
  );

  useEffect(() => {
    const host: string = "ws://broker.emqx.io:8083/mqtt";
    const options: IClientOptions = {
      protocol: "ws",
      clientId: "string123",
    };
    const brokerUrl = "ws://194.233.64.65:9001";
    const client = mqtt.connect(brokerUrl);
    // const client = mqtt.connect(host, options);

    client.on("connect", () => {
      console.log("Connected to MQTT broker");
      client.subscribe("temp", { qos: 0 }, (err) => {
        if (err) console.log(err);
      });
    });

    client.on("error", (err) => {
      console.error("Connection error: ", err);
      client.end();
    });

    const generateRandomTwoDigitNumber = () =>
      Math.floor(Math.random() * 90) + 10;

    const publishInterval = setInterval(() => {
      const temperature = generateRandomTwoDigitNumber();
      const humidity = generateRandomTwoDigitNumber();

      // client.publish("temperature", JSON.stringify(temperature), {
      //   qos: 0,
      // });
      // client.publish("humidity", JSON.stringify(humidity), { qos: 0 });
    }, 100);

    client.on("message", (topic, payload) => {
      if (topic === "temperature") {
        // console.log(message);
        // setTempData((prevData) => {
        //   const newTempLabels = [...prevData.labels, newDatelabel];
        //   const newTempData = [
        //     ...prevData.datasets[0].data,
        //     parseInt(message),
        //   ].slice(-100);
        //   return {
        //     labels: newTempLabels,
        //     datasets: [{ ...prevData.datasets[0], data: newTempData }],
        //   };
        // });
      } else if (topic === "temp") {
        const tempData: { val: string; timestamp: string } = JSON.parse(
          payload.toString()
        );
        const currentTime = new Date();
        const message = tempData.val;
        const newDatelabel = `${moment(tempData.timestamp).format(
          "hh:mm:ss A"
        )}(${moment(currentTime).format("hh:mm:ss A")})`;
        // (${moment(currentTime).format("hh:mm:ss A")})
        // console.log(message, newDatelabel);

        setCentHumidityData((prevData) => {
          const newLabels = [...prevData.labels, newDatelabel];
          const newData = [...prevData.datasets[0].data, parseInt(message)];
          return {
            labels: newLabels,
            datasets: [{ ...prevData.datasets[0], data: newData }],
          };
        });
        setData((prevData) => {
          const newLabels = [...prevData.labels, newDatelabel].slice(-40);
          const newData = [
            ...prevData.datasets[0].data,
            parseInt(message),
          ].slice(-40);
          return {
            labels: newLabels,
            datasets: [{ ...prevData.datasets[0], data: newData }],
          };
        });
      }
    });

    return () => {
      // clearInterval(publishInterval);
      client.unsubscribe("test/temperature", (error) => {
        if (error) {
          console.log("Unsubscribe error", error);
        }
      });
      client.end(); // Clean up the client on component unmount
    };
  }, []);

  useEffect(() => {
    processData(
      centHumidityData,
      setCentHumidityData,
      humDataArray,
      setHumDataArray
    );
  }, [centHumidityData, processData]);

  useEffect(() => {
    // processData(tempData, setTempData, tempDataArray, setTempDataArray);
  }, [tempData, processData]);

  const options: ChartOptions<"line"> = {
    responsive: true,
    maintainAspectRatio: false,
    interaction: {
      mode: "index",
      intersect: false,
    },
    plugins: {
      legend: {
        display: true,

        position: "top",
        labels: { font: { size: 14, weight: 700 }, color: "black" },
      },
      title: {
        display: true,
        text: "Temperature",
        color: "black",
        font: { size: 16 },
      },
    },
    animations: {
      tension: {
        duration: 1000,
        easing: "linear",
        from: 0.4,
        to: 0.4,
        loop: false,
      },
      x: {
        duration: 0,
        delay: 0,
      },
      y: {
        duration: 0,
        easing: "easeOutCubic",
        delay: 0,
      },
    },
    scales: {
      x: {
        display: true,
        title: {
          display: true,
          text: "Time Stamp",
          font: { size: 12, weight: 600 },
          color: "black",
        },
        grid: {
          display: true,
          drawTicks: true,
          drawOnChartArea: true,
          tickLength: 2,
          // color: "white",
        },
        ticks: {
          maxTicksLimit: 30,
          font: {
            size: 10,
            weight: 600,
          },
          color: "black",
        },
      },
      y: {
        min: 0,
        max: 4500,
        display: true,
        title: {
          display: true,
          text: "Temperature In Celsius",
          font: { size: 12, weight: 600 },
          color: "black",
        },
        grid: {
          display: true,
          drawOnChartArea: true,
          // color: "white",
        },
        ticks: {
          stepSize: 30,
          font: {
            size: 10,
            weight: 600,
          },
          color: "black",
        },
      },
    },
  };

  useEffect(() => {
    console.log(humDataArray);
  }, [humDataArray.length]);

  return (
    <>
      <div className="garph_main_cards">
        <div className="humidity_graph_card">
          <Line data={data} options={options} />
        </div>
        {/* <div className="temp_graph_card">
          <Line data={tempData} options={options} />
        </div> */}
        {/* <button onClick={() => setStop(false)}>Start Again</button> */}
      </div>
      <div style={{ overflow: "auto", display: "flex" }}>
        {humDataArray.map((data, index) => (
          <div key={index}>
            <Line
              style={{ width: "600px" }}
              data={{
                labels: data.humLabelData,
                datasets: [
                  {
                    label: `Humidity on ${moment(newDate).format(
                      "DD/MM/YYYY"
                    )} `,
                    data: data.humData,
                    fill: true,
                    backgroundColor: "rgba(75,192,192,0.2)",
                    borderColor: "rgba(75,192,192,1)",
                  },
                ],
              }}
            />
          </div>
        ))}
      </div>
      <div style={{ overflow: "auto", display: "flex" }}>
        {tempDataArray.map((data, index) => (
          <div key={index}>
            <Line
              data={{
                labels: data.humLabelData,
                datasets: [
                  {
                    label: `Temperature on ${moment(newDate).format(
                      "DD/MM/YYYY"
                    )} `,
                    data: data.humData,
                    fill: true,
                    backgroundColor: "rgba(238, 158, 199, 0.31)",
                    borderColor: "#FF76CE",
                  },
                ],
              }}
            />
          </div>
        ))}
      </div>
    </>
  );
};

export default MQTTChart;
