import React, { FC } from "react";
import LabeledCheckbox from "./Components/Checkbox/LabeledCheckbox";

interface Testing {
  children: React.ReactNode;
}

export const Test: FC<Testing> = (props) => {
  const { children } = props;
  console.log(children);

  const child1 = React.Children.toArray(children).find(
    (child) => React.isValidElement(child) && child.type === LabeledCheckbox
  );
  return (
    <div>
      <h1>Hello CodeSandbox</h1>
      {child1}
      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
};
