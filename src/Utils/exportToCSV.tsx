import { Columns, Rows } from "../Components/Table/ITable";

export const exportToCSV = (columns: Columns, rows: Rows) => {
  if (rows.length !== 0) {
    const csvData: string[][] = [];
    const header = columns.map((col) => col.headerName);
    csvData.push(header as string[]);
    rows.forEach((row) => {
      const rowData: any[] = [];
      columns.forEach((col) => {
        rowData.push(row[col.field]);
      });
      csvData.push(rowData);
    });
    // create csv content
    const csvContent =
      "data:text/csv;charset=utf-8," +
      csvData.map((e) => e.join(",")).join("\n");
    const encodedUri = encodeURI(csvContent);
    // create download link
    const link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "data.csv");
    // Append link to body and trigger click event
    document.body.appendChild(link);
    link.click();
  } else {
    alert("No data found");
  }
};
