import { jsPDF } from "jspdf";
import { Columns, Rows } from "../Components/Table/ITable";
import "jspdf-autotable";

declare module "jspdf" {
  interface jsPDF {
    autoTable: (options: any) => jsPDF;
  }
}

export const exportToPDF = (columns: Columns, rows: Rows) => {
  if (rows.length !== 0) {
    // create jspdf object
    const doc = new jsPDF();
    // header and data
    const header = columns.map((col) => col.headerName);
    const data = rows.map((row) => columns.map((col) => row[col.field]));

    doc.autoTable({
      head: [header],
      body: data,
    });

    doc.save("data.pdf");
  } else {
    alert("No data found");
  }
};
