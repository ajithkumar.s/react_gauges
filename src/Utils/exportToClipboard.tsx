import { Columns, Rows } from "../Components/Table/ITable";

export const copyToClipboard = (columns: Columns, rows: Rows) => {
  if (rows.length !== 0) {
    const columnHeaders = Object.keys(rows[0]).join("\t");
    const dataRows = rows
      .map((row) => Object.values(row).join("\t"))
      .join("\n");
    const dataToCopy = columnHeaders + "\n" + dataRows;

    // window navigate to access clipboard object
    navigator.clipboard
      .writeText(dataToCopy)
      .then(() => {
        alert("Data copied to clipboard");
      })
      .catch((err) => {
        console.error("Failed to copy:", err);
      });
  } else {
    alert("No data found");
  }
};
