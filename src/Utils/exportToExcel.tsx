import * as XLSX from "xlsx";
import { Columns, Rows } from "../Components/Table/ITable";

export const exportToExcel = (columns: Columns, rows: Rows) => {
  if (rows.length !== 0) {
    // file type, name, extension
    const fileType =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";
    const fileName = "exported_data";

    const columnHeaders = columns.map((column) => column.headerName);
    const fields = Object.keys(rows[0]);
    const sheetData = rows.map((row) =>
      fields.map((field) => (row[field] ? row[field] : ""))
    );
    sheetData.unshift(columnHeaders);
    console.log(sheetData);

    // create data to excel format
    const ws = XLSX.utils.json_to_sheet(sheetData, { skipHeader: true });
    // create workbook
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    // convert workbook to binary excel buffer
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    // create blob object
    const data = new Blob([excelBuffer], { type: fileType });

    // Create download link
    const url = window.URL.createObjectURL(data);
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", fileName + fileExtension);
    // Append link to body and trigger click event
    document.body.appendChild(link);
    link.click();
  } else {
    alert("No data found");
  }
};
