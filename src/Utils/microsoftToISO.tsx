export const microsoftToISOFormater = (dateString: string) => {
  const timestamp = parseInt(dateString.match(/\d+/)![0], 10);
  const offset = parseInt(dateString.match(/[+-]\d+/)![0], 10);

  const dt = new Date(timestamp + offset * 60 * 1000);
  const iso_formate = dt.toISOString();
  return iso_formate;
};
